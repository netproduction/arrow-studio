<?php
	/**
	 * Blog view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>
		<div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<?php /* foreach ($feed as $post): ?>
								<div class="post">
									<h4><?php echo $post['title']; ?></h4>
									<?php echo $post['description']; ?>
								</div>
							<?php endforeach; */ ?>
							<iframe id="middle-box" class="middle-box text-center" src="<?=$link ?>" width="100%" height="600px"></iframe>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/footer.sec.php') ?>
			</div>
		</div>
	</body>
</html>
