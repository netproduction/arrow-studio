<?php
	/**
	 * Password view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>
		<div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="middle-box" class="middle-box text-center">
								<?=nl2h($lang['MANAGER_TITLE'], "2") ?>
								</h2>
								<br>
								<?php include_once("views/sections/manager-password-form.sec.php"); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>