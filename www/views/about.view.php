<?php
	/**
	 * About view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>
		<div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="middle-box" class="middle-box text-center about-content">
								<!-- <h2 class="text-center">
									<?=$lang['ABOUT_TITLE'] ?>
								</h2>
								<br> -->
								<?=nl2h($lang['ABOUT_TEXT'], "4") ?>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/footer.sec.php') ?>
			</div>
		</div>
	</body>
</html>
