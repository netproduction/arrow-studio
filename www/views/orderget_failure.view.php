<?php
    /**
     * Order failure view
     * @author Antoine De Gieter
     */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
    <?php include_once('views/sections/head.sec.php') ?>

    <body>
        <div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="middle-box" class="middle-box text-center failure-content">
								<!-- <h2 class="text-center">
									<?=$lang['FAILURE_TITLE'] ?>
								</h2>
								<br> -->
								<?=nl2h(ucfirst(strtolower($lang['FAILURE_TEXT'])), "4", "center") ?>
                                <p class="text-center">
                                    <a href="?page=cart" class="link-black"><?=$lang['FAILURE_LINK_CART_TEXT'] ?></a>
                                </p>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/footer.sec.php') ?>
			</div>
		</div>
    </body>
</html>
