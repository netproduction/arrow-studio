<?php
	/**
	 * Item view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>
		<div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div id="middle-box" class="middle-box text-center">
								<div class="col-md-5 col-lg-5 hidden-xs hidden-sm">
									<a href="uploads/img/items/<?=$item["pictures"][0]['item'] ?>-<?=$item["pictures"][0]['picture'] ?>.jpg" id="link-lightbox" data-lightbox="gallery">
										<div id="big-picture" style="background-image:url(uploads/img/items/<?=$item["pictures"][0]['item'] ?>-<?=$item["pictures"][0]['picture'] ?>.jpg);">
										</div>
									</a>
								</div>

								<div class="col-md-1 col-lg-1 hidden-xs hidden-sm">
									<div style="width:100%; height:500px;">
										<?php foreach($item["pictures"] as $picture): ?>
										<div class="text-center">
											<img class="thumbnail-picture" src="uploads/img/items/<?=$picture['item'] ?>-<?=$picture['picture'] ?>.jpg" alt="Picture" width="100%">
											<br>
											<br>
										</div>
										<?php endforeach; ?>
									</div>
								</div>

								<div class="col-md-3 col-lg-3 hidden-md hidden-lg">
									<?php foreach($item["pictures"] as $picture): ?>
									<div class="text-center">
										<a href="uploads/img/items/<?=$picture['item'] ?>-<?=$picture['picture'] ?>.jpg" id="link-lightbox" data-lightbox="gallery">
											<img class="thumbnail-picture" src="uploads/img/items/<?=$picture['item'] ?>-<?=$picture['picture'] ?>.jpg" alt="Picture" width="100%">
										</a>
										<br>
										<br>
									</div>
									<?php endforeach; ?>
								</div>

								<div class="col-md-6 col-lg-6">
									<?=nl2h($item['name']) ?>
									<?=nl2p(utf8_encode($item['description'])) ?>
									<br>

									<br>

									<br>
									<div id="form-add-to-cart" class="text-left">
										<?php if ($item['has_size']): ?>
											<input type="radio" name="size-radio" value="S" id="S"> <label for="S">S</label>
											<input type="radio" name="size-radio" value="M" id="M" checked> <label for="M">M</label>
											<input type="radio" name="size-radio" value="L" id="L"> <label for="L">L</label>
											<br>
										<?php else: ?>
											<br>
											<input type="radio" style="display:none;" name="size-radio" value="-" checked>
										<?php endif; ?>

										<?php if ($item['sales']): ?>
											<small><strike class="item-initial-price">CHF <?=$item['initial_price'] ?>.-</strike></small><br>
										<?php endif; ?>

										<strong class="item-price">CHF <?=$item['price'] ?>.- </strong><br>
										<br>

										<button class="btn btn-default add-item" id="add-item" type="button" data-item-id="<?=$item['id']; ?>" data-item-price="<?=$item['price']; ?>" data-item-name="<?=$item['name']; ?>">
											<?=$lang['SHOP_ADD_TO_CART']; ?>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php include_once('views/sections/footer.sec.php') ?>
			</div>
		</div>

		<script type="text/javascript">
			$('.thumbnail-picture').on('click', function(e) {
				$('#big-picture').css('background-image', 'url(' + this.src + ')');
				// var src = this.src;
				// var selected = $(this).val();
				// var image = $("#big-picture");
				// image.fadeOut('fast', function () {
				// 	image.attr('src', src);
				// 	image.fadeIn('fast');
				// });
				$('#link-lightbox').attr('href', this.src);
			});
		</script>
	</body>
</html>
