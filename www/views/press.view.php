<?php
	/**
	 * Press view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>
		<div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<?php /* <object data="global/pdf/press_<?=$lang['LANG'] ?>.pdf" type="application/pdf" id="middle-box" class="middle-box text-center">
								<embed src="global/pdf/press_<?=$lang['LANG'] ?>.pdf" type="application/pdf" />
							</object> */ ?>

							<div id="middle-box" class="middle-box text-center">
								<?php $count = 0; ?>
								<p class="text-center">
								<?php foreach ($images as $image): ?>
									<a href="<?=$image['src'] ?>" data-lightbox="gallery">
										<img width="100%" src="<?=$image['src'] ?>"><br>
									</a>
								<?php endforeach; ?>
								</p>

								<h4 class="text-center">
									<a href="global/pdf/press_<?=$lang['LANG'] ?>.pdf" target="_blank" class="link-black" download><?=$lang['BUTTON_DOWNLOAD'] ?></a>
								</h4>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/footer.sec.php') ?>
			</div>
		</div>
		<script type="text/javascript" src="global/js/arrows.js"></script>
	</body>
</html>
