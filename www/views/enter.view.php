<?php
	/**
	 * Default home view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>

		<div id="fullpage">

			<div class="section" id="enter" data-anchor="enter">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-lg-4 hidden-lg hidden-md">
							<p class="text-center">
								<br>
						 		<img src="global/img/logos/logo.png" alt="Logo" class="logo">
							</p>
						</div>

						<div class="hidden-lg hidden-md">
							<p class="text-center">
								<br>
								<a href="?lang=en-GB" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/lang-en.png" width="26" height="26" alt="English (United Kingdom)">
								</a>
								<a href="?lang=fr-CH" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/lang-fr.png" width="26" height="26" alt="Français (Suisse)">
								</a>
							</p>
						</div>

						<div class="col-md-4 col-lg-4 hidden-sm hidden-xs">
							<p class="text-center">
								<br>
								<a href="?lang=en-GB" class="pull-left" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/lang-en.png" width="26" height="26" alt="English (United Kingdom)">
								</a>
								<a href="?lang=fr-CH" class="pull-left" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/lang-fr.png" width="26" height="26" alt="Français (Suisse)">
								</a>
							</p>
						</div>

						<div class="col-md-4 col-lg-4 hidden-xs hidden-sm">
							<p class="text-center">
								<br>
						 		<img src="global/img/logos/logo.png" alt="Logo" class="logo">
							</p>
						</div>

						<div class="hidden-lg hidden-md">
						 	<p class="text-center">
								<br>
								<a href="http://www.instagram.com/arrowstudiolausanne" target="_blank" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/social-instagram.png" alt="Instagram" width="26" height="26">
								</a>
						        <a href="https://www.facebook.com/arrowstudiolausanne" target="_blank" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/social-facebook.png" alt="Facebook" width="26" height="26">
								</a>
						        <a href="http://www.arrowstudiolausanne.tumblr.com" target="_blank" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/social-tumblr.png" alt="Tumblr" width="26" height="26">
								</a>
						        <a href="https://www.pinterest.com/arrowstudioCH/" target="_blank" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/social-pinterest.png" alt="Pinterest" width="26" height="26">
								</a>
						 	</p>
						</div>

						<div class="col-md-4 col-lg-4 hidden-xs hidden-sm">
						 	<p class="text-center">
								<br>
								<a href="http://www.instagram.com/arrowstudiolausanne" class="pull-right" target="_blank" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/social-instagram.png" alt="Instagram" width="26" height="26">
								</a>
						        <a href="https://www.facebook.com/arrowstudiolausanne" class="pull-right" target="_blank" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/social-facebook.png" alt="Facebook" width="26" height="26">
								</a>
						        <a href="http://www.arrowstudiolausanne.tumblr.com" class="pull-right" target="_blank" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/social-tumblr.png" alt="Tumblr" width="26" height="26">
								</a>
						        <a href="#" class="pull-right" target="_blank" style="margin-left:4px; margin-right:4px">
									<img src="global/img/icons/social-pinterest.png" alt="Pinterest" width="26" height="26">
								</a>
						 	</p>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="enter-middle-box text-center" id="enter-middle-box">
								<div class="instagram-stream-container">
									<?php $count = 0; ?>
									<?php foreach ($feed as $in => $post): $count++ ?>
										<?php if (1 === $count % 2): ?>
											<div class="row">
										<?php endif; ?>
											<div class="col-md-6 col-lg-6 <?php if ($count % 2) echo 'text-right'; else echo 'text-left'; ?>">
												<img src="<?= $post['img'] ?>" alt="Instagram picture" class="instagram-img">
											</div>
										<?php if (0 === $count % 2): ?>
											</div>
										<?php endif; ?>
									<?php endforeach; ?>
									<?php if (0 !== $count % 2): ?>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/menu-bottom.sec.php') ?>
			</div>

			<div class="section" data-anchor="collections">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="alternate-middle-box text-center" id="alternate-middle-box">
								<?php $count = 0; ?>
								<?php foreach ($collections as $image): ?>
									<?php if (0 === $count % 2): ?>
										<div class="row">
									<?php endif; ?>
											<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
												<a href="<?=$image['src'] ?>" data-lightbox="gallery">
													<img width="100%" src="<?=$image['src'] ?>"><br>
												</a>
												<br>
											</div>
									<?php if (1 === $count++ % 2): ?>
										</div>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/footer.sec.php') ?>
			</div>

		</div>

		<script type="text/javascript" src="global/js/arrows.js"></script>
	</body>
</html>
