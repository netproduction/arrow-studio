<?php
    /**
     * Shop view
     * @author Antoine De Gieter
     */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
    <?php include_once('views/sections/head.sec.php') ?>

    <body>
        <div id="fakeloader"></div>
        <div id="fullpage">
            <div class="section">
                <?php include_once('views/sections/menu-top.sec.php') ?>
                <?php include_once('views/sections/shop-categories.sec.php'); ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div id="middle-box" class="middle-box">
                                <div class="shop-stream-container"></div>
                                <?php $count = 0; ?>
                                <?php foreach ($itemsBySubcategory as $subcategory => $items): ?>
                                    <?php foreach ($items as $item): ?>
                                        <?php if (1 === ++$count % 4): ?>
                                            <div class="row">
                                        <?php endif; ?>
                                            <div class="col-md-3 col-lg-3">
                                                <div class="shop-item category-<?=$category ?> text-center">
                                                    <a href="?page=item&amp;item=<?=$item['id'] ?>"><img src="uploads/img/items/<?=$item['id'] ?>-1.jpg" alt="item-<?=$item['id']; ?>" class="img-thumbnail item-img"></a><br>
                                                    <h5 class="text-center">
                                                            <a href="?page=item&amp;item=<?=$item['id'] ?>" class="link-black">
                                                            <?=$item['name'] ?>
                                                        </a>
                                                        <br>
                                                        CHF <?=$item['price'] ?>.-
                                                    </h5>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php if (0 === $count % 4): ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                                <?php if (0 !== $count % 4): ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include_once('views/sections/footer.sec.php') ?>
            </div>
        </div>

        <?php include_once('views/sections/checkout.sec.php') ?>

        <script type="text/javascript" src="global/js/cart-actions.js"></script>
        <script type="text/javascript" src="global/js/arrows.js"></script>
    </body>
</html>
