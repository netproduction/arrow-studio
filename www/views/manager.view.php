<?php
    /**
     * Manager view
     * @author Antoine De Gieter
     */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
    <?php include_once('views/sections/head.sec.php') ?>

    <body>
        <div id="fullpage">
            <div class="section">
                <?php include_once('views/sections/menu-top.sec.php') ?>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div id="middle-box" class="middle-box text-center">
                                <?=nl2h($lang['MANAGER_TITLE'], "2") ?>
                                <?php switch ($alert) {
                                    case "created": ?>
                                <div class="alert alert-success">
                                    <span class="glyphicon glyphicon-star"></span>
                                    Article créé !
                                </div>
                                <?php break;

                                    case "deleted": ?>
                                <div class="alert alert-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    Article supprimé !
                                </div>
                                <?php break;

                                    case "shipped": ?>
                                <div class="alert alert-info">
                                    <span class="glyphicon glyphicon-send"></span>
                                    Commande envoyée !
                                </div>
                                <?php break;

                                    case "cancelled": ?>
                                <div class="alert alert-danger">
                                    <span class="glyphicon glyphicon-ban-circle"></span>
                                    Commande annulée !
                                </div>
                                <?php break;

                                    default: ?>
                                <?php break; ?>
                                <?php } ?>
                                <br>
                                <?php include_once("views/sections/manager-tabs.sec.php"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(".action-cancel").on('click', function(event) {
                console.log(event.srcElement);
                window.open('?page=manager&action=cancel&id=' + event.srcElement.dataset.basketId);
            });
            $(".action-bill").on('click', function(event) {
                console.log(event.srcElement);
                window.open('?page=manager&action=bill&id=' + event.srcElement.dataset.basketId);
            });
        </script>
    </body>
</html>
