<?php
	/**
	 * Collections view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>
		<div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
							<div id="middle-box" class="middle-box text-center">
								<?php $count = 0; ?>
								<?php foreach ($images as $image): ?>
									<?php if (0 === $count % 2): ?>
										<div class="row">
									<?php endif; ?>
											<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
												<a href="<?=$image['src'] ?>" data-lightbox="gallery">
													<img width="100%" src="<?=$image['src'] ?>"><br>
												</a>
												<br>
											</div>
									<?php if (1 === $count++ % 2): ?>
										</div>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/footer.sec.php') ?>
			</div>
		</div>
		<script type="text/javascript" src="global/js/arrows.js"></script>
	</body>
</html>
