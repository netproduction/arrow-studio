<?php
	/**
	 * Checkout view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>
        <div id="fakeloader"></div>
		<div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="middle-box" class="middle-box text-center checkout-content">
								<!-- <h2 class="text-center">
									<?=$lang['CHECKOUT_TITLE'] ?>
								</h2>
								<br> -->
                                <div class="row">
                					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                						<h6 class="text-left">
                							<strong><?=$lang['CART_TOTAL']; ?></strong><br>
                							CHF <span id="checkout-cart-total"></span>.-
                						</h6>
                					</div>

                					<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                        <?php if ("captcha" === $error): ?>
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            Captcha!
                                        </div>
                                        <?php endif; ?>
                						<form method="post" action="" id="checkout-form" class="text-left">
                                            <?php /* foreach($lang['CHECKOUT_TITLES'] as $title): ?>
                                                <input type="radio" value="<?=$title ?>" id="title-<?=$title ?>" name="checkout-title"> <label for="title-<?=$title ?>"><?=$title ?></label>
                                            <?php endforeach; */ ?>

                							<div class="row">
                                                <div class="col-md-4 col-lg-4">
                								    <input type="text" placeholder="<?=$lang['CHECKOUT_FIRST_NAME'] ?>" class="form-control" name="checkout-first-name" id="checkout-first-name">
                                                </div>
                                                <div class="col-md-4 col-lg-4">
                								    <input type="text" placeholder="<?=$lang['CHECKOUT_LAST_NAME'] ?>" class="form-control" name="checkout-last-name" id="checkout-last-name">
                                                </div>
                                                <div class="col-md-4 col-lg-4">
				                                    <input type="email" placeholder="<?=$lang['CHECKOUT_EMAIL'] ?>" class="form-control" name="checkout-email" id="checkout-email">
                                                </div>
                							</div>

                                            <hr>

                							<label for="checkout-billing-address"><?=$lang['CHECKOUT_BILLING_ADDRESS'] ?></label>
                							<textarea class="form-control" id="checkout-billing-address" name="checkout-billing-address" rows="3"></textarea><br>

                							<button type="button" class="btn btn-default btn-block" onclick="javascript:$('#checkout-shipping-address').html($('#checkout-billing-address').val());"><?=$lang['CHECKOUT_COPY'] ?> &darr;</button><br>

                							<label for="checkout-shipping-address"><?=$lang['CHECKOUT_SHIPPING_ADDRESS'] ?></label>
                							<textarea class="form-control" id="checkout-shipping-address" name="checkout-shipping-address" rows="3"></textarea>

                                            <hr>

                                            <div class="row">
                                                <div class="col-md-4 col-lg-4">
                                                    <div class="g-recaptcha" data-sitekey="6Lf2gwgTAAAAAL6UNfZ0GSQDiO2F3fhPQqQoi2SQ"></div>
                                                </div>

                                                <div class="col-md-4 col-lg-4">
                                                    <?php
                                    					switch ($lang['LANG']) {
                                    						case 'fr-CH':
                                    				?>
                                    				<!-- PayPal Logo --><table border="0" cellpadding="10" cellspacing="0" align="center"><tbody><tr><td align="center"></td></tr><tr><td align="center"><a href="https://www.paypal.com/fr/webapps/mpp/paypal-popup" title="PayPal Comment Ca Marche" onclick="javascript:window.open('https://www.paypal.com/fr/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/logo-center/logo_paypal_securise_fr.png" border="0" alt="Securise par PayPal" /></a><div style="text-align:center"><a href="https://www.paypal.com/fr/webapps/mpp/why" target="_blank"><font size="2" face="Arial" color="#0079CD"><strong>PayPal Comment Ca Marche</strong></font></a></div></td></tr></tbody></table>
													<!-- PayPal Logo -->
                                    				<?php
                                    						break;

                                    						case 'en-GB':
                                    				?>
                                    				<!-- PayPal Logo --><table border="0" cellpadding="10" cellspacing="0" align="center"><tr><td align="center"></td></tr><tr><td align="center"><a href="https://www.paypal.com/uk/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/uk/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/logo/bdg_secured_by_pp_2line.png" border="0" alt="Secured by PayPal"></a><div style="text-align:center"><a href="https://www.paypal.com/uk/webapps/mpp/how-paypal-works" target="_blank" ><font size="2" face="Arial" color="#0079CD"><b>How PayPal Works</b></font></a></div></td></tr></table>
													<!-- PayPal Logo -->
                                    				<?php
                                    						break;
                                    					}
                                    				?>
                                                </div>

                                                <div class="col-md-4 col-lg-4">
                                                    <p class="text-right">
                                    					<button type="button" data-dismiss="modal" aria-hidden="true" onclick="javascript:launchFakeLoader(); $('#checkout-form').submit();" class="btn btn-default btn-block"><span class="glyphicon glyphicon-shopping-cart"></span> <?=$lang['CHECKOUT_PROCEED'] ?></button>
                                    				</p>
                                                </div>
                                            </div>

											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<p class="text-center">
														<small>
															Payment is in US $. Visa™, MasterCard™, and American Express™ accepted. Paypal™ and Discover™ available for purchases in USD $.
														</small>
													</p>
												</div>
											</div>

                						</form>
                					</div>
                				</div>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/footer.sec.php') ?>
			</div>
		</div>

        <script type="text/javascript" src="global/js/cart-actions.js"></script>
	</body>
</html>
