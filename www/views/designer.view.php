<?php
	/**
	 * Designer view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>
		<div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="middle-box" class="middle-box text-center designer-content">
								<div class="vertical-white-space"></div>
								<div class="row">
									<div class="col-md-1 col-lg-1"></div>
									<div class="col-md-11 col-lg-11">
										<?=nl2h($lang['DESIGNER_TITLE'], "2", "justify", "text-shadow:0px 0px 6px rgb(255, 255, 255);") ?>
									</div>
								</div>
								<div class="vertical-small-white-space"></div>
								<div class="row">
									<div class="col-md-6 col-lg-6"></div>
									<div class="col-md-6 col-lg-6">
										<div class="vertical-white-space"></div>
										<?=nl2h($lang['DESIGNER_TEXT'], "4", "justify") ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/footer.sec.php') ?>
			</div>
		</div>
	</body>
</html>
