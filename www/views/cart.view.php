<?php
    /**
     * Cart view
     * @author Antoine De Gieter
     */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
    <?php include_once('views/sections/head.sec.php') ?>

    <body>
        <div id="fakeloader"></div>
        <div id="fullpage">
            <div class="section">
                <?php include_once('views/sections/menu-top.sec.php') ?>
                <?php include_once('views/sections/shop-categories.sec.php'); ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div id="middle-box" class="middle-box">

                                <?php /* <div style="font-size:1.58em;">
                                    <a href="?page=shop" class="btn btn-link link-black" style="font-size:0.4em;">&laquo; <?=$lang['CART_BACK_TO_SHOP'] ?></a>
                                </div> */ ?>

                                <?=nl2h($lang['MENU_CART']) ?>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="font-family:'Light'; font-weight:normal;">
                                                Item
                                            </th>
                                            <th style="font-family:'Light'; font-weight:normal;">
                                                Description
                                            </th>
                                            <th style="font-family:'Light'; font-weight:normal;" class="text-center">
                                                Price
                                            </th>
                                            <th style="font-family:'Light'; font-weight:normal;" class="text-center">
                                                Quantity
                                            </th>
                                            <th style="font-family:'Light'; font-weight:normal;" class="text-right">
                                                Subtotal
                                            </th>
                                            <th style="font-family:'Light'; font-weight:normal;" class="text-right">
                                                Action
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody id="cart-contents">
                                    </tbody>

                                    <tfoot>
                                        <tr>
                                            <th style="font-family:'Light'; font-weight:normal;" colspan="3"></th>
                                            <th style="font-family:'Light'; font-weight:normal;" class="text-right">
                                                Total
                                            </th>
                                            <th style="font-family:'Light'; font-weight:normal;" class="text-right">
                                                CHF <span id="cart-total"></span>.-
                                            </th>
                                            <th style="font-family:'Light'; font-weight:normal;"></th>
                                        </tr>
                                    </tfoot>
                                </table>

                                <p class="text-right">
                                    <!--label for="promo-input">Promo code</label> <input type="text" id="promo-input" name="promo-input" style="background:#fff;border:1px solid #000;"><br-->
                            	    <a class="btn btn-default" <?php /*data-toggle="modal" */ ?> href="?page=checkout"><?=$lang['CART_CHECKOUT'] ?> </a>
                            	    <?php /* <button type="button" class="btn btn-sm empty"><span class="glyphicon glyphicon-trash"></span> <?=$lang['CART_EMPTY'] ?> </button> */ ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include_once('views/sections/footer.sec.php') ?>
            </div>
        </div>

        <?php include_once('views/sections/checkout.sec.php') ?>

        <script type="text/javascript" src="global/js/cart-actions.js"></script>
        <script type="text/javascript" src="global/js/arrows.js"></script>
    </body>
</html>
