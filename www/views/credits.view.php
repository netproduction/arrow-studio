<?php
	/**
	 * Credits view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>
		<div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="middle-box" class="middle-box text-center credits-content">
								<?=nl2h($lang['CREDITS_TITLE'], "2") ?>
								<br>
                                <?php foreach($lang['CREDITS_CONTENTS'] as $cred) : ?>
                                    <?=nl2h($cred['title'], "3") ?>
                                    <?php foreach($cred['people'] as $people): ?>
                                    <p class="text-left">
                                    	<?php if (!empty($people['url'])): ?>
                                        <a href="<?=$people['url'] ?>" class="link-black">
                                        <?php endif; ?>
                                        <?=$people['name'] ?><br>
                                       	<?php if (!empty($people['url'])): ?>
                                        </a>
                                        <?php endif; ?>
                                    </p>
                                    <?php endforeach; ?>
								<br>
                            <?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/footer.sec.php') ?>
			</div>
		</div>
	</body>
</html>
