<ul class="nav navbar-nav navbar-center social-icons">
    <li>
        <a href="http://www.instagram.com/arrowstudiolausanne" target="_blank" style="padding-left:4px; padding-right:4px; margin-left:0px; margin-right:0px;"><img src="global/img/icons/social-instagram.png" alt="Instagram" width="26" height="26" /></a>
    </li>

    <li>
        <a href="https://www.facebook.com/arrowstudiolausanne" target="_blank" style="padding-left:4px; padding-right:4px; margin-left:0px; margin-right:0px;"><img src="global/img/icons/social-facebook.png" alt="Facebook" width="26" height="26" /></a>
    </li>

    <li>
        <a href="http://www.arrowstudiolausanne.tumblr.com" target="_blank" style="padding-left:4px; padding-right:4px; margin-left:0px; margin-right:0px;"><img src="global/img/icons/social-tumblr.png" alt="Tumblr" width="26" height="26" /></a>
    </li>

    <li>
        <a href="https://www.pinterest.com/arrowstudioCH/" target="_blank" style="padding-left:4px; padding-right:4px; margin-left:0px; margin-right:0px;"><img src="global/img/icons/social-pinterest.png" alt="Pinterest" width="26" height="26" /></a>
    </li>
</ul>
