<?php
	/**
	 * Top bar section
	 * @author Antoine De Gieter
	 */
?>

<nav class="navbar navbar-white navbar-static-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">

		<ul class="nav navbar-nav navbar-center">
			<a class="navbar-brand" href="?page=enter"><img src="global/img/logos/arrow.jpg" alt="Logo" class="logo"></a>
		</ul>

		<ul class="nav navbar-nav navbar-right">
			<li>
				<?php include_once('views/sections/languages.sec.php') ?>
			</li>
		</ul>

	</div><!-- /.navbar-collapse -->
</nav>