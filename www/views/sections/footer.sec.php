<nav class="navbar navbar-white footer hidden-xs hidden-sm">
	<div class="row">
		<div class="col-md-4 col-lg-4">
			<ul class="nav navbar-nav">
				<li>
					<a class="link-black" href="?page=legal" style="font-size: 1.1em"><?=$lang['FOOTER_LEGAL_NOTICE'] ?></a>
				</li>
			</ul>
		</div>

		<div class="col-md-4 col-lg-4">
			<div class="text-center">
				<?php include('views/sections/social.sec.php') ?>
			</div>
		</div>

		<div class="col-md-4 col-lg-4">
			<ul class="nav navbar-nav navbar-right">
				<li>
					<?php include('views/sections/languages.sec.php') ?>
				</li>
				<li style="margin-right: 12px;">
					<a class="link-black" href="?page=credits" style="display:inline-block; font-size:1.1em; padding-left:4px; padding-right:4px; margin-left:0px; margin-right:0px;"><?=$lang['FOOTER_CREDITS'] ?></a>
					/
					<a class="link-black" href="?page=press" style="display:inline-block; font-size:1.1em; padding-left:4px; padding-right:4px; margin-left:0px; margin-right:0px;"><?=$lang['FOOTER_PRESS'] ?></a>
				</li>
			</ul>
		</div>
	</div>
</nav>


<nav class="navbar navbar-white footer hidden-md hidden-lg">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">

			<div class="row text-center">
				<?php include('views/sections/social.sec.php') ?>
			</div>

			<div class="row text-center">
				<ul class="nav navbar-center text-center">
					<li>
						<a class="link-black" href="#"><?=$lang['FOOTER_CREDITS'] ?></a>
					</li>
					<li>
						<a class="link-black" href="?page=press"><?=$lang['FOOTER_PRESS'] ?></a>
					</li>
				</ul>
			</div>

			<div class="row text-center">
				<ul class="nav text-center">
					<li>
						<a class="link-black" href="#"><?=$lang['FOOTER_LEGAL_NOTICE'] ?></a>
					</li>
				</ul>
			</div>

		</div>
	</div>
</nav>
