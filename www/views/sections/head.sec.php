<?php
    /**
     * Default head section
     * @author Antoine De Gieter
     * @author Jean Mercadier
     */
?>

<head>
    <meta charset="utf-8">
    <title><?php echo DEFAULT_TITLE; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- FAVICON -->
    <link rel="icon" type="image/png" href="global/img/icons/fav.png">

    <!-- STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="global/css/third-party/bootstrap.min.css"> <!-- https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css -->
    <link rel="stylesheet" type="text/css" href="global/css/third-party/fakeLoader.css">

    <link rel="stylesheet" type="text/css" href="global/css/theme.css">
    <link rel="stylesheet" type="text/css" href="global/css/main.css">
    <link rel="stylesheet" type="text/css" href="global/css/menu.css">


    <!-- Targeting watches and other wearables
    <link rel="stylesheet" type="text/css" href="global/css/extra-small/main.css">
    <link rel="stylesheet" type="text/css" href="global/css/extra-small/menu.css"-->

    <link rel="stylesheet" type="text/css" href="global/css/small/main.css" media="screen and (max-width: 767px)">
    <link rel="stylesheet" type="text/css" href="global/css/small/menu.css" media="screen and (max-width: 767px)">

    <link rel="stylesheet" type="text/css" href="global/css/medium/main.css" media="screen and (min-width: 768px) and (max-width: 991px)">
    <link rel="stylesheet" type="text/css" href="global/css/medium/menu.css" media="screen and (min-width: 768px) and (max-width: 991px)">

    <link rel="stylesheet" type="text/css" href="global/css/large/main.css" media="screen and (min-width: 992px) and (max-width: 1439px)">
    <link rel="stylesheet" type="text/css" href="global/css/large/menu.css" media="screen and (min-width: 992px) and (max-width: 1439px)">

    <link rel="stylesheet" type="text/css" href="global/css/extra-large/main.css" media="screen and (min-width: 1440px)">
    <link rel="stylesheet" type="text/css" href="global/css/extra-large/menu.css" media="screen and (min-width: 1440px)">

    <link rel="stylesheet" type="text/css" href="global/css/third-party/lightbox.css">
    <link rel="stylesheet" type="text/css" href="global/css/third-party/jquery.fullPage.css">

    <!-- JAVASCRIPT -->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script><!-- //code.jquery.com/jquery-1.11.3.min.js -->
    <script type="text/javascript" src="global/js/third-party/lightbox.js"></script>
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script><!-- https://www.google.com/recaptcha/api.js -->
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="global/js/js.cookie.js"></script>
    <script type="text/javascript" src="global/js/alert-handler.js"></script>
    <script type="text/javascript" src="global/js/cart-manager.js"></script>
    <script type="text/javascript" src="global/js/cart-actions.js"></script>
    <script type="text/javascript" src="global/js/fakeLoader.min.js"></script>
    <script type="text/javascript" src="global/js/slimscroll.js"></script>
    <script type="text/javascript" src="global/js/jquery.fullPage.min.js"></script>
    <script type="text/javascript" src="global/js/menu.js"></script>
    <script type="text/javascript" src="global/js/main.js"></script>
</head>
