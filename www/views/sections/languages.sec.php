<ul class="nav navbar-nav navbar-center" id="languages">
	<li>
		<a href="?lang=en-GB" style="padding-right:4px; padding-left:4px; margin-right:0px; margin-left:0px;">
			<img src="global/img/icons/lang-en.png" width="26" height="26" alt="English (United Kingdom)">
		</a>
	</li>

	<li>
		<a href="?lang=fr-CH" style="padding-right:4px; padding-left:4px; margin-right:0px; margin-left:0px;">
			<img src="global/img/icons/lang-fr.png" width="26" height="26" alt="Français (Suisse)">
		</a>
	</li>
</ul>
