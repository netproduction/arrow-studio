<div class="tabbable tabs-right">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab-1" class="link-black" data-toggle="tab">Commandes</a></li>
		<li><a href="#tab-2" class="link-black" data-toggle="tab">Articles</a></li>
		<li><a href="#tab-3" class="link-black" data-toggle="tab">Ajouter un article</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab-1">
			<?php include_once("views/sections/tabs/1.tab.php"); ?>
		</div>
		<div class="tab-pane" id="tab-2">
			<?php include_once("views/sections/tabs/2.tab.php"); ?>
		</div>
		<div class="tab-pane" id="tab-3">
			<?php include_once("views/sections/tabs/3.tab.php"); ?>
		</div>
	</div>
</div>
