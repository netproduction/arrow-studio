<form action="" method="POST" role="form">

	<?=nl2h($lang['MANAGER_RESTRICTED_ACCESS'], "3"); ?>

	<div class="form-group">
		<label class="sr-only" for="manager-password"><?=$lang['MANAGER_PASSWORD']; ?></label>
		<input type="password" id="manager-password" name="manager-password" class="form-control" placeholder="<?=$lang['MANAGER_PASSWORD'] ?>">
	</div>

	<button type="submit" class="btn btn-primary"><?=$lang['MANAGER_SUBMIT'] ?></button>
</form>