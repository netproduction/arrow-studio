<?php
	/**
	 * Default menu section
	 * @author Antoine De Gieter
	 */
?>

<nav class="navbar navbar-white navbar-static-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<h3><img src="global/img/logos/a.png" alt="Toggle menu"> &nbsp;Menu</h3>
		</button>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">

		<!--ul class="nav navbar-nav text-center">

		</ul-->

		<ul class="nav navbar-nav hidden-xs hidden-sm navbar-center" style="margin-left:-32px;">
			<li><a class="link-black menu-item" href="?page=collections"><?=$lang['MENU_COLLECTIONS'] ?></a></li>
			<li id="shop-menu-label" class="link-black menu-item"><a class="link-black" href="?page=shop"><?=$lang['MENU_SHOP'] ?></a>
				<ul class="sub-nav hidden-xs hidden-sm" style="width:320%;">

					<li class="menu-item" id="category-pap">
						<a class="link-black sub-menu" href="#" id="link-pap"><?=$lang['MENU_SHOP_PAP'] ?></a>
						<ul style="display:none;" id="menu-pap" class="menu-subcategories">
							<li><a class="link-black sub-sub-menu" href="?page=shop&amp;filter=tshirt"><?=$lang['MENU_SHOP_TSHIRT'] ?></a></li><br>
							<li><a class="link-black sub-sub-menu" href="?page=shop&amp;filter=sweatshirt"><?=$lang['MENU_SHOP_SWEATSHIRT'] ?></a></li><br>
							<li><a class="link-black sub-sub-menu" href="?page=shop&amp;filter=skirt"><?=$lang['MENU_SHOP_SKIRT'] ?></a></li>
						</ul>
					</li>

					<li class="menu-item" id="category-acc">
						<a class="link-black sub-menu" href="#" id="link-acc"><?=$lang['MENU_SHOP_ACC'] ?></a>
						<ul style="display:none;" id="menu-acc" class="menu-subcategories">
							<li><a class="link-black sub-sub-menu" href="?page=shop&amp;filter=bag"><?=$lang['MENU_SHOP_BAG'] ?></a>
						</ul>
					</li>

				</ul>
			</li>

			<li><a class="link-black menu-item" href="?page=universe"><?=$lang['MENU_UNIVERSE'] ?></a></li>

			<li><a class="navbar-brand" href="?page=enter"><img src="global/img/logos/logo.png" alt="Logo" class="logo"></a></li>

			<li><a class="link-black menu-item hidden-md hidden-lg" href="?page=philosophy"><?=$lang['MENU_PHILOSOPHY'] ?></a></li>
			<li><a class="link-black menu-item hidden-md hidden-lg" href="?page=designer"><?=$lang['MENU_DESIGNER'] ?></a></li>
			<li id="shop-menu-label" class="link-black menu-item"><a class="link-black" href="#"><?=$lang['MENU_ABOUT'] ?></a>
				<ul class="sub-nav">
					<li><a class="link-black sub-menu" href="?page=philosophy"><?=$lang['MENU_PHILOSOPHY'] ?></a></li>
					<li><a class="link-black sub-menu" href="?page=designer"><?=$lang['MENU_DESIGNER'] ?></a></li>
				</ul>
			</li>
			<li><a class="link-black menu-item" href="?page=blog"><?=$lang['MENU_BLOG'] ?></a></li>
			<li><a class="link-black menu-item" href="?page=contact"><?=$lang['MENU_CONTACT'] ?></a></li>
		</ul>

		<ul class="nav navbar-nav navbar-right text-center hidden-md hidden-lg">
			<li><a class="link-black menu-item" href="?page=philosophy"><?=$lang['MENU_PHILOSOPHY'] ?></a></li>
			<li><a class="link-black menu-item" href="?page=designer"><?=$lang['MENU_DESIGNER'] ?></a></li>
			<li><a class="link-black menu-item" href="?page=blog"><?=$lang['MENU_BLOG'] ?></a></li>
			<li><a class="link-black menu-item" href="?page=contact"><?=$lang['MENU_CONTACT'] ?></a></li>
			<li><a class="link-black menu-item" href="?page=cart"><span class="glyphicon glyphicon-shopping-cart"></span> (<span class="cart-count"></span>)</a></li>
		</ul>

		<!--ul class="nav navbar-nav navbar-right text-center hidden-xs hidden-sm">

		</ul-->

		<div class="hidden-md hidden-lg text-center">
			<?php include('views/sections/languages.sec.php') ?>
		</div>

	</div><!-- /.navbar-collapse -->
</nav>

<?php include('views/sections/cart-link.sec.php') ?>
