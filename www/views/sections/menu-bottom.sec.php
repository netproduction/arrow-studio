<?php
	/**
	 * Default menu section
	 * @author Antoine De Gieter
	 */
?>

<nav class="navbar navbar-white enter-navbar hidden-xs hidden-sm" role="navigation">
	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">

		<ul class="nav navbar-nav navbar-center">
			<li><a href="?page=collections"><?=$lang['MENU_COLLECTIONS'] ?></a></li>
			<li><a href="?page=shop"><?=$lang['MENU_SHOP'] ?></a></li>
			<li><a href="?page=universe"><?=$lang['MENU_UNIVERSE'] ?></a></li>

			<a class="navbar-brand" id="enter_arrow" href="#collections" onclick="javascript:$.fn.fullpage.moveSectionDown();"><img alt="Enter" src="global/img/icons/down-arrow.png" width="32px" height="32px"></a>

			<li><a href="?page=about"><?=$lang['MENU_ABOUT'] ?></a></li>
			<li><a href="?page=blog"><?=$lang['MENU_BLOG'] ?></a></li>
			<li><a href="?page=contact"><?=$lang['MENU_CONTACT'] ?></a></li>
			<li><a href="#"></a></li>
		</ul>

	</div><!-- /.navbar-collapse -->
</nav>

<div class="hidden-lg hidden-md">
	<p class="text-center">
		<br>
		<a href="?page=ucollections" style="color:black; font-size:2.9em;"><img alt="Enter" src="global/img/icons/down-arrow.png" width="18px" height="18px"></a>
	</p>
</div>
