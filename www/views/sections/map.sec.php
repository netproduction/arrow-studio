<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<div style="overflow:hidden;height:100%;width:100%;" class="hidden-xs hidden-sm">
	<div id="gmap_canvas" style="height:100%;width:100%;"></div>
	<style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
</div>
<script type="text/javascript">
	function init_map(){
		var myOptions = {
			zoom:17,
			center:new google.maps.LatLng(46.52523,6.62401),
			mapTypeId: google.maps.MapTypeId.SATELLITE
		};
		map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
		marker = new google.maps.Marker({
			map: map,position: new google.maps.LatLng(46.52523,6.62401)
		});
		infowindow = new google.maps.InfoWindow({
			content:"<b>Arrow Studio</b><br/>Avenue de France<br/>1004 Lausanne"
		});
		google.maps.event.addListener(marker, "click", function(){
			//infowindow.open(map,marker);
		});
		//infowindow.open(map,marker);
	}

	google.maps.event.addDomListener(window, 'load', init_map);
</script>
