<div id="cart">
	<h6>
		<a href="?page=cart" class="link-black"><?=$lang['CART_CONTENTS'] ?> (<span id="cart-count"></span>)</a> :
	</h6>

	<div id="cart-contents"></div><br>

	<p>
		<strong><?=$lang['CART_TOTAL'] ?></strong><br>
		CHF <span id="cart-total"></span>.-
	</p>

	<button type="button" class="btn btn-default" data-toggle="modal" href="#modal-checkout"><span class="glyphicon glyphicon-ok"></span> <?=$lang['CART_CHECKOUT'] ?> </button><br>
	<br>
	<button type="button" class="btn btn-danger btn-xs empty"><span class="glyphicon glyphicon-trash"></span> <?=$lang['CART_EMPTY'] ?> </button>
</div>
