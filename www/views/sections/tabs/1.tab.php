<div class="table-responsive">
	<a href="http://paypal.com/" class="link-black" target="_blank">Vérifier les paiements sur Paypal</a>
	<table class="table table-hover table-bordered table-striped">
		<thead>
			<tr>
				<th class="text-left"></th>
				<th class="text-left">Référence</th>
				<th class="text-left">État</th>
				<th class="text-left">Date</th>
				<th class="text-left">Acheteur</th>
				<th class="text-left">Contenu du panier</th>
				<th class="text-left">Total</th>
			</tr>
		</thead>

		<tbody>
			<?php foreach ($carts as $cart): ?>
				<?php
					switch ($cart['state']):
						case 'pending':
							$actions = array(
								"contact",
								"cancel"
							);
							$state = "En attente";
							$glyphicon = "flag";
							$label = "warning";
							break;
						case 'paid':
							$actions = array(
								"contact",
								"send",
								"bill",
								"cancel"
							);
							$state = "Payé";
							$glyphicon = "shopping-cart";
							$label = "primary";
							break;
						case 'shipped':
							$actions = array(
								"contact",
								"bill"
							);
							$state = "Envoyé";
							$glyphicon = "ok";
							$label = "success";
							break;
						case 'cancelled':
							$actions = array(
								"contact"
							);
							$state = "Annulé";
							$glyphicon = "remove";
							$label = "danger";
							break;
					endswitch;
				?>
				<tr>
					<!-- Action -->
					<td class="text-left">
						<div class="btn-group">
							<a class="btn dropdown-toggle btn-default" data-toggle="dropdown" href="#"><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php if (in_array("send", $actions)): ?>
								<li><a href="?page=manager&amp;action=send&amp;object=cart&amp;id=<?=$cart['id'] ?>" class="action-send" data-id="<?=$cart['id'] ?>">Envoyer la commande</a></li>
								<?php endif; ?>

								<?php if (in_array("contact", $actions)): ?>
								<li><a href="mailto:<?=$cart['email'] ?>">Contacter le client</a></li>
								<?php endif; ?>

								<?php if (in_array("bill", $actions)): ?>
								<li><a target="_blank" href="?page=manager&amp;action=bill&amp;object=cart&amp;id=<?=$cart['id'] ?>">Afficher la facture</a></li>
								<?php endif; ?>

								<?php if (in_array("cancel", $actions)): ?>
								<li class="divider"></li>
								<li><a href="?page=manager&amp;action=cancel&amp;object=cart&amp;id=<?=$cart['id'] ?>">Annuler la commande</a></li>
								<?php endif; ?>
							</ul>
						</div>
					</td>

					<!-- # -->
					<td class="text-left lead">
						#<?=$cart['id'] ?>
					</td>

					<!-- State -->
					<td class="text-left">
						<span class="label label-<?=$label ?>"><?=$state ?></span>
					</td>

					<!-- Date -->
					<td class="text-left">
						<?=$lang['DAY_OF_THE_WEEK'][date('N', strtotime($cart['dt'])) - 1] ?>
						<?=date('d', strtotime($cart['dt'])) ?>
						<?=$lang['MONTH'][date('n', strtotime($cart['dt'])) - 1] ?>
						<?=date('Y', strtotime($cart['dt'])) ?>
					</td>

					<!-- Customer -->
					<td class="text-left">
						<strong><a href="mailto:<?=$cart['email'] ?>" class="link-black"><?=$cart['c_name'] ?></a></strong><br>
						<small><?=nl2p($cart['s_addr']) ?></small>
					</td>

					<!-- Items -->
					<td class="text-left">
						<?php foreach ($cart['items'] as $item): ?>
							<?=$item['quantity'] ?> &times; <?=$item['name'] ?>
							<?php if ("-" !== $item['size']): ?>
							: <strong><?=$item['size'] ?></strong>
							<?php endif; ?>
							<br>
						<?php endforeach; ?>
					</td>

					<!-- Total -->
					<td class="text-left">
						<strong>CHF <?=$cart['total'] ?>.-</strong>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
