<form method="post" enctype="multipart/form-data" action="?page=manager&amp;action=create&amp;object=item">
    <legend>Article</legend>

    <input type="hidden" name="add-item">

    <div class="row">
        <div class="col-md-3 col-lg-3">
            <select name="item-subcategory" class="form-control">
                <optgroup name="Prêt-à-porter" label="Prêt-à-porter" title="Prêt-à-porter">
                    <option value="tshirt">T-Shirt</option>
                    <option value="sweatshirt">Sweatshirt</option>
                    <option value="skirt">Jupe</option>
                </optgroup>
                <optgroup name="Accessoires" label="Accessoires" title="Accessoires">
                    <option value="bag">Sac</option>
                </optgroup>
            </select>
        </div>
        <div class="col-md-3 col-lg-3">
            <input type="text" class="form-control" name="item-name" id="add-item-name" placeholder="Nom de l'article">
        </div>
        <div class="col-md-4 col-lg-4">
            <input type="file" name="item-img[]" id="add-item-img" multiple>
        </div>
        <div class="col-md-2 col-lg-2 text-right">
            <div class="hidden">
                <input type="checkbox" name="item-sales" id="add-item-sales">
                <label for="add-item-sales">en solde</label>
            </div>
            <input type="number" min="0" class="form-control" name="item-price" id="add-item-price" placeholder="Prix">
        </div>
    </div>

    <div class="hidden">
        <hr>

        <div class="row">
            <div class="col-md-6 col-lg-6">
                <input type="hidden" value="0" min="0" class="form-control" name="item-initial-price" id="add-item-initial-price" placeholder="Prix initial">
            </div>
            <div class="col-md-6 col-lg-6">

            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-6 col-lg-6">
            <textarea class="form-control" name="item-text-fr" placeholder="Détails (français)"></textarea>
        </div>
        <div class="col-md-6 col-lg-6">
            <textarea class="form-control" name="item-text-en" placeholder="Détails (anglais)"></textarea>
        </div>
    </div>

    <hr>

    <button type="submit" class="btn btn-default btn-block">Créer</button>
</form>
