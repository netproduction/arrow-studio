<table class="table table-bordered">
    <thead>
        <tr>
            <th class="text-left">Categorie</th>
            <th class="text-left">Nom</th>
            <!--th class="text-left">Prix initial</th-->
            <th class="text-left">Prix</th>
            <th class="text-left">Photo</th>
            <!-- <th class="text-left">Soldes</th> -->
            <th class="text-center">…</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($itemsByCategory as $category): ?>
            <?php if (0 !== count($category['items'])): ?>
            <tr>
                <td class="text-left lead" style="vertical-align:middle;" rowspan="<?=count($category['items']) ?>"><?=$category['subcategory'] ?></td>
                <td class="text-left" style="vertical-align:middle;"><strong><?=$category['items'][0]['name'] ?></strong></td>
                <?php /* td class="text-left" style="vertical-align:middle;"><strong>CHF <?=$category['items'][0]['initial_price'] ?>.-</strong></td */ ?>
                <td class="text-left" style="vertical-align:middle;"><strong>CHF <?=$category['items'][0]['price'] ?>.-</strong></td>
                <td class="text-left" style="vertical-align:middle;"><img src="uploads/img/items/<?=$category['items'][0]['id']; ?>-1.jpg" alt="item-<?=$category['items'][0]['id']; ?>" class="img-thumbnail manager-item-img" width="150px"></td>
                <?php /* td class="text-left" style="vertical-align:middle;"><input type="checkbox" class="form-control" name="checkbox-soldes" data-id="<?=$category['items'][0]['id']; ?>" class="checkbox-soldes"></td> */ ?>
                <td class="text-center" style="vertical-align:middle;"><a href="?page=manager&amp;action=delete&amp;object=item&amp;id=<?=$item['id']; ?>" onclick="javascript:return confirm('Suppression de <?=$item['name'] ?> ! Confirmer l'action ?');" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a></td>
            </tr>
            <?php array_shift($category['items']); ?>
            <?php foreach ($category['items'] as $item): ?>
            <tr>
                <td class="text-left" style="vertical-align:middle;"><strong><?=$item['name'] ?></strong></td>
                <td class="text-left" style="vertical-align:middle;"><strong>CHF <?=$item['initial_price'] ?>.-</strong></td>
                <td class="text-left" style="vertical-align:middle;"><strong>CHF <?=$item['price'] ?>.-</strong></td>
                <td class="text-left" style="vertical-align:middle;"><img src="uploads/img/items/<?=$item['id']; ?>-1.jpg" alt="item-<?=$category['items'][0]['id']; ?>" class="img-thumbnail manager-item-img" width="150px"></td>
                <td class="text-left" style="vertical-align:middle;"><input type="checkbox" class="form-control" name="checkbox-soldes" data-id="<?=$category['items'][0]['id']; ?>" class="checkbox-soldes"></td>
                <td class="text-center" style="vertical-align:middle;"><a href="?page=manager&amp;action=delete&amp;object=item&amp;id=<?=$item['id']; ?>" onclick="javascript:return confirm('Suppression de <?=$item['name'] ?> ! Confirmer l'action ?');" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></a></td>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>
