<div id="cart-link" class="hidden-sm hidden-xs">
	<a class="link-black" href="?page=cart"><span class="glyphicon glyphicon-shopping-cart"></span> (<span class="cart-count"></span>)</a>
</div>