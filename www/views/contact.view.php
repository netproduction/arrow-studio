<?php
	/**
	 * Contact view
	 * @author Antoine De Gieter
	 */
?>

<!doctype html>
<html lang="<?=$lang['LANG']; ?>">
	<?php include_once('views/sections/head.sec.php') ?>

	<body>
		<div id="fullpage">
			<div class="section">
				<?php include_once('views/sections/menu-top.sec.php') ?>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="middle-box" class="middle-box text-center">
								<?php include_once('views/sections/map.sec.php') ?>

								<form class="contact-form" action="" method="POST" role="form">
								 	<p class="text-center">
										<legend>Contact Us</legend>

										<div class="form-group">
											<!-- <label for="contact-email">E-mail :</label> -->
											<input type="email" id="contact-email" class="form-control" name="contact-email" placeholder="Votre adresse e-mail">
										</div>

										<div class="form-group">
											<textarea id="contact-message" class="form-control" name="contact-message" placeholder="Votre message" rows="5"></textarea>
										</div>



										<button type="submit" class="btn btn-default">Envoyer</button>
										<div class="contact-info-row">
											<span>
												<img src="global/img/icons/mail.png" alt="mail" /><br>
												<a class="link-black" href="mailto:info@arrowstudio.ch">info@arrowstudio.ch</a>
											</span>

											<span style="margin-left:12px; margin-right:12px;">
												Arrow Studio<br>Rue du Jura 11,<br>1004 Lausanne<br>Sur rendez-vous
											</span>

											<span>
												<img src="global/img/icons/tel.png" alt="tel" /><br>
												<a class="link-black" href="tel:+41786054850">+41 78 605 48 50</a>
											</span>
										</div>
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
				<?php include_once('views/sections/footer.sec.php') ?>
			</div>
		</div>
	</body>
</html>
