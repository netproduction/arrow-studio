<?php
    class Customer {
        public static function get($email) {
            $dbh = SPDO::getInstance();
            $stmt = $dbh->prepare("SELECT id FROM customer WHERE email = :email;");
            $stmt->bindParam(":email", $email, PDO::PARAM_STR);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            return $row;
        }

        public static function create($email, $title, $first_name, $last_name,
            $billing_address, $shipping_address, $language) {
            $dbh = SPDO::getInstance();

            #$exists = self::get($email);
            $stmt = $dbh->prepare("INSERT INTO customer (email, title, first_name,
                last_name, billing_address, shipping_address, language)
                VALUES (:email, :title, :first_name, :last_name, :billing_address,
                :shipping_address, :language);");
            $stmt->bindParam(":email", $email, PDO::PARAM_STR);
            $stmt->bindParam(":title", $title, PDO::PARAM_STR);
            $stmt->bindParam(":first_name", $first_name, PDO::PARAM_STR);
            $stmt->bindParam(":last_name", $last_name, PDO::PARAM_STR);
            $stmt->bindParam(":billing_address", $billing_address, PDO::PARAM_STR);
            $stmt->bindParam(":shipping_address", $shipping_address, PDO::PARAM_STR);
            $stmt->bindParam(":language", $language, PDO::PARAM_STR);
            // $stmt->bindParam(":password", $password, PDO::PARAM_STR);
            $stmt->execute();
            $stmt->closeCursor();
            $customerId = $dbh->lastInsertId();
            return $customerId;
        }
    }
