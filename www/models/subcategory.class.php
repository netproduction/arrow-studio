<?php
	class Subcategory {
		public static function getAll($language = "fr-CH") {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("SELECT sc.id AS id, sct.text AS name FROM subcategory sc INNER JOIN subcategory_tr sct ON sc.id = sct.subcategory WHERE sct.language = :language;");
			$stmt->bindParam(":language", $language, PDO::PARAM_STR);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$stmt->closeCursor();
			return utf8_converter($rows);
		}

		public static function get($key, $language = "fr-CH") {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("SELECT sc.id AS id, sct.text AS name FROM subcategory sc INNER JOIN subcategory_tr sct ON sc.id = sct.subcategory WHERE sc.key = :key AND sct.language = :language;");
			$stmt->bindParam(":key", $key, PDO::PARAM_STR);
			$stmt->bindParam(":language", $language, PDO::PARAM_STR);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$stmt->closeCursor();
			return utf8_converter($rows);
		}
	}
