<?php
	class Cart {
		public static function get($id, $language = "fr-CH") {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("SELECT ca.id, ca.billing_address AS b_addr,
				ca.shipping_address AS s_addr, ca.state AS state,
				cu.email AS email, CONCAT(cu.first_name, ' ', cu.last_name) AS c_name, ca.dt AS dt
				FROM cart ca INNER JOIN customer cu ON ca.customer = cu.id
				WHERE ca.id = :id;");
			$stmt->bindParam(":id", $id, PDO::PARAM_INT);
			$stmt->execute();
			$cart = $stmt->fetch(PDO::FETCH_ASSOC);

			$stmt = $dbh->prepare("SELECT i.id, i.price, it.text AS name,
				c.quantity, (c.quantity * i.price) AS subtotal, c.size AS size
				FROM (item i INNER JOIN item_tr it ON i.id = it.item)
				INNER JOIN contains c ON i.id = c.item
				WHERE c.cart = :cartId AND it.language = :language");
			$stmt->bindParam(":cartId", $id, PDO::PARAM_INT);
			$stmt->bindParam(":language", $language, PDO::PARAM_STR);
			$stmt->execute();
			$cart['items'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$stmt->closeCursor();

			$total = 0;
			foreach ($cart['items'] as $item)
				$total += $item['subtotal'];
			$cart['total'] = $total;
			return utf8_converter($cart);
		}

		public static function create($billingAddress, $shippingAddress, $customerId, $items) {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("INSERT INTO cart (billing_address, shipping_address, customer) VALUES (:billing_address, :shipping_address, :customerId);");
			$stmt->bindParam(":billing_address", $billingAddress, PDO::PARAM_STR);
			$stmt->bindParam(":shipping_address", $shippingAddress, PDO::PARAM_STR);
			$stmt->bindParam(":customerId", $customerId, PDO::PARAM_INT);
			$stmt->execute();
			$stmt->closeCursor();
			$cartId = $dbh->lastInsertId();
			self::addItems($cartId, $items);
			return $cartId;
		}

		public static function addItems($cartId, $items) {
			$dbh = SPDO::getInstance();
			foreach ($items as $item) {
				$itemId = $item[0];
				$itemQuantity = $item[1];
				$itemSize = $item[2];
				$stmt = $dbh->prepare("SELECT quantity FROM contains WHERE cart = :cartId AND item = :itemId AND size = :size;");
				$stmt->bindParam(":cartId", $cartId, PDO::PARAM_INT);
				$stmt->bindParam(":itemId", $itemId, PDO::PARAM_INT);
				$stmt->bindParam(":size", $itemSize, PDO::PARAM_STR);
				$stmt->execute();
				if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$quantity = $row['quantity'] + 1;
					$stmt = $dbh->prepare("UPDATE contains SET quantity = :quantity WHERE cart = :cartId AND item = :itemId AND size = :size;");
				} else {
					$stmt = $dbh->prepare("INSERT INTO contains VALUES (:cartId, :itemId, :quantity, :size);");
				}
				$stmt->bindParam(":cartId", $cartId, PDO::PARAM_INT);
				$stmt->bindParam(":itemId", $itemId, PDO::PARAM_INT);
				$stmt->bindParam(":quantity", $itemQuantity, PDO::PARAM_INT);
				$stmt->bindParam(":size", $itemSize, PDO::PARAM_STR);
				$stmt->execute();
				$stmt->closeCursor();
			}
		}

		public static function updateState($id, $state) {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("UPDATE cart SET state = :state WHERE id = :id");
			$stmt->bindParam(":id", $id, PDO::PARAM_INT);
			$stmt->bindParam(":state", $state, PDO::PARAM_STR);
			$stmt->execute();
			$stmt->closeCursor();
		}

		public static function getAll($state = "all", $language = "fr-CH") {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("SELECT id FROM cart WHERE state LIKE :state
				ORDER BY state,dt ASC;");
			if ("all" === $state)
				$stmt->bindValue(":state", "%%", PDO::PARAM_STR);
			else
				$stmt->bindParam(":state", "%" . $state . "%", PDO::PARAM_STR);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$stmt->closeCursor();
			$carts = Array();
			foreach ($rows as $row) {
				$carts[] = self::get($row['id'], $language);
			}
			return utf8_converter($carts);
		}

		public static function update($id, $initialPrice, $price, $categoryId) {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("UPDATE item SET initial_price = :initialPrice, price = :price, category = :categoryId WHERE id = :id;");
			$stmt->bindParam(":id", id, PDO::PARAM_INT);
			$stmt->bindParam(":initialPrice", $initialPrice, PDO::PARAM_STR);
			$stmt->bindParam(":price", $price, PDO::PARAM_STR);
			$stmt->bindParam(":categoryId", $initialPrice, PDO::PARAM_INT);
			$stmt->execute();
			$stmt->closeCursor();
		}

		public static function delete($id) {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("DELETE FROM cart WHERE id = :id;");
			$stmt->bindParam(":id", id, PDO::PARAM_INT);
			$stmt->execute();
			$stmt->closeCursor();
		}
	}
