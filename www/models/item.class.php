<?php
	class Item {
		public static function get($id, $language = "fr-CH") {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("SELECT it.text AS name, sct.text AS category,
				i.id AS id, it.description AS description,
				i.price AS price, i.initial_price AS initial_price,
				i.sales AS sales, c.key AS category
				FROM category_tr ct
				INNER JOIN category c ON ct.category = c.id
				INNER JOIN subcategory sc ON sc.category = c.id
				INNER JOIN subcategory_tr sct ON sct.subcategory = sc.id
				INNER JOIN item i ON i.subcategory = sc.id
				INNER JOIN item_tr it ON i.id = it.item
				WHERE i.id = :id
				AND sct.language = :language
				AND it.language = :language;");
			$stmt->bindParam(":id", $id, PDO::PARAM_INT);
			$stmt->bindParam(":language", $language, PDO::PARAM_STR);
			$stmt->execute();
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			$row['has_size'] = $row['category'] === "pretaporter";

			$stmt = $dbh->prepare("SELECT * FROM item_picture WHERE item = :id;");
			$stmt->bindParam(":id", $id, PDO::PARAM_INT);
			$stmt->execute();
			$row["pictures"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$stmt->closeCursor();
			return $row;
		}

		public static function create($name, $initialPrice, $price, $subcategory, $sales, $textFR, $textEN) {
			$dbh = SPDO::getInstance();

			$subcategory_id = Subcategory::get($subcategory)[0]['id'];

			$stmt = $dbh->prepare("INSERT INTO item (initial_price, price, sales, subcategory) VALUES (:initialPrice, :price, :sales, :subcategoryId);");
			$stmt->bindParam(":initialPrice", $initialPrice, PDO::PARAM_INT);
			$stmt->bindParam(":price", $price, PDO::PARAM_INT);
			$stmt->bindParam(":sales", $sales, PDO::PARAM_INT);
			$stmt->bindValue(":subcategoryId", $subcategory_id, PDO::PARAM_INT);
			$stmt->execute();
			$item_id = $dbh->lastInsertId();

			$stmt = $dbh->prepare("INSERT INTO item_tr (item, language, `text`, description)
				VALUES (:item, 'fr-CH', :nameFR, :textFR),
				(:item, 'en-GB', :nameEN, :textEN);");
			$stmt->bindParam(":item", $item_id, PDO::PARAM_INT);
			$stmt->bindParam(":nameFR", $name, PDO::PARAM_STR);
			$stmt->bindParam(":nameEN", $name, PDO::PARAM_STR);
			$stmt->bindParam(":textFR", $textFR, PDO::PARAM_STR);
			$stmt->bindParam(":textEN", $textEN, PDO::PARAM_STR);
			$stmt->execute();
			$stmt->closeCursor();
			return $item_id;
		}

		public static function addPicture($item, $img) {
			$dbh = SPDO::getInstance();

			$stmt = $dbh->prepare("INSERT INTO item_picture (item, picture) VALUES (:item, :picture);");
			$stmt->bindParam(":item", $item, PDO::PARAM_INT);
			$stmt->bindParam(":picture", $img, PDO::PARAM_INT);
			$stmt->execute();
			$stmt->closeCursor();
		}

		public static function getAll() {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("SELECT * FROM item;");
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$stmt->closeCursor();
			return $rows;
		}

		public static function getBySubcategory($subcategory, $language = "fr-CH") {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("SELECT i.id AS id,
				i.initial_price AS initial_price,
				i.price AS price, it.text AS name
				FROM item i INNER JOIN item_tr it
				ON i.id = it.item
				WHERE i.subcategory = :subcategoryId AND it.language = :language");
			$stmt->bindParam(":subcategoryId", $subcategory, PDO::PARAM_INT);
			$stmt->bindParam(":language", $language, PDO::PARAM_STR);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$stmt->closeCursor();
			return $rows;
		}

		public static function update($id, $initialPrice, $price, $subcategoryId) {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("UPDATE item SET initial_price = :initialPrice, price = :price, subcategory = :subcategoryId WHERE id = :id;");
			$stmt->bindParam(":id", id, PDO::PARAM_INT);
			$stmt->bindParam(":initialPrice", $initialPrice, PDO::PARAM_STR);
			$stmt->bindParam(":price", $price, PDO::PARAM_STR);
			$stmt->bindParam(":subcategoryId", $subcategoryId, PDO::PARAM_INT);
			$stmt->execute();
			$stmt->closeCursor();
		}

		public static function delete($id) {
			$dbh = SPDO::getInstance();
			$stmt = $dbh->prepare("DELETE FROM item WHERE id = :id;");
			$stmt->bindParam(":id", $id, PDO::PARAM_INT);
			$stmt->execute();
			$stmt->closeCursor();
		}
	}
