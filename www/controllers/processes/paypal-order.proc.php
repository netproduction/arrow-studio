<?php
    /**
     * Processes paypal order
     * @author Antoine De Gieter
     * @copyright Net Production Köbe & Co
     */

    require_once __DIR__ . '/../../lib/paypal-bootstrap.inc.php';
    use PayPal\Api\Amount;
    use PayPal\Api\Details;
    use PayPal\Api\Item;
    use PayPal\Api\ItemList;
    use PayPal\Api\Payer;
    use PayPal\Api\Payment;
    use PayPal\Api\RedirectUrls;
    use PayPal\Api\Transaction;

    // ### Payer
    // A resource representing a Payer that funds a payment
    // For paypal account payments, set payment method
    // to 'paypal'.
    $payer = new Payer();
    $payer->setPaymentMethod("paypal");

    $list = Array();

    foreach ($cart['items'] as $item) {
        $tmp_item = new Item();
        $tmp_item->setName($item['name'])
        ->setCurrency('CHF')
        ->setQuantity($item['quantity'])
        ->setPrice($item['price']);
        $list[] = $tmp_item;
    }

    $itemList = new ItemList();
    $itemList->setItems($list);

    // ### Additional payment details
    // Use this optional field to set additional
    // payment information such as tax, shipping
    // charges etc.
    $details = new Details();
    $details->setShipping(0)
        ->setTax(0)
        ->setSubtotal($cart['total']);

    // ### Amount
    // Lets you specify a payment amount.
    // You can also specify additional details
    // such as shipping, tax.
    $amount = new Amount();
    $amount->setCurrency("CHF")
        ->setTotal($cart['total'])
        ->setDetails($details);

    // ### Transaction
    // A transaction defines the contract of a
    // payment - what is the payment for and who
    // is fulfilling it.
    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setItemList($itemList)
        ->setDescription("Arrow Studio Order")
        ->setInvoiceNumber(uniqid());

    // ### Redirect urls
    // Set the urls that the buyer must be redirected to after
    // payment approval/ cancellation.
    // $baseUrl = getBaseUrl();
    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl("http://127.0.0.1/arrow-studio/www/?callback=orderget&success=true&cart=". $cartId)
        ->setCancelUrl("http://127.0.0.1/arrow-studio/www/?callback=orderget&success=false&cart=". $cartId);

    // ### Payment
    // A Payment Resource; create one using
    // the above types and intent set to 'order'
    $payment = new Payment();
    $payment->setIntent("order")
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));

    // For Sample Purposes Only.
    $request = clone $payment;

    // ### Create Payment
    // Create a payment by calling the 'create' method
    // passing it a valid apiContext.
    // (See bootstrap.php for more on `ApiContext`)
    // The return object contains the state and the
    // url to which the buyer must be redirected to
    // for payment approval
    try {
        $payment->create($apiContext);
    } catch (Exception $ex) {
        print_r($ex);
        exit(1);
    }

    // ### Get redirect url
    // The API response provides the url that you must redirect
    // the buyer to. Retrieve the url from the $payment->getApprovalLink()
    // method
    $approvalUrl = $payment->getApprovalLink();
    header("Location: $approvalUrl");

    // return $payment;
