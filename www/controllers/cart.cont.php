<?php
    /**
     * Cart front controller
     * @author Antoine De Gieter
     */

    // Fetch items
    $subcategories = Subcategory::getAll($lang['LANG']);

    $itemsBySubcategory = Array();
    foreach ($subcategories as $subcategory) {
        $itemsBySubcategory[$subcategory['id']] = utf8_converter(Item::getBySubcategory($subcategory['id'], $lang['LANG']));
    }

    /* Process payment */
    if (isset($_POST['g-recaptcha-response'])) {

        /* Check reCaptcha */
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $fields = Array(
            "secret" => urlencode("6Lf2gwgTAAAAAL7DgYM38Tax6uOssC2qFeRDueog"),
            "response" => urlencode($_POST['g-recaptcha-response'])
        );

        $fields_string = "";
        foreach($fields as $key => $value) {
            $fields_string .= $key . "=" . $value . "&";
        }
        rtrim($fields_string, '&');

        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_POST, count($fields));
        curl_setopt($request, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($request);
        curl_close($request);
        $response = json_decode($result);

        if ($response->success) {
            $cart = json_decode($_COOKIE['cart']);
            print_r($cart);
            $token = $cart->token;

            $itemList = Array();
            foreach ($cart->items as $item) {
                $itemList[] = Array(
                    $item->id,
                    $item->quantity,
                    $item->itemsize
                );
            }

            $email = htmlspecialchars($_POST['checkout-email']);
            $title = htmlspecialchars($_POST['checkout-title']);
            $firstName = htmlspecialchars($_POST['checkout-first-name']);
            $lastName = htmlspecialchars($_POST['checkout-last-name']);
            $billingAddress = htmlspecialchars($_POST['checkout-billing-address']);
            $shippingAddress = htmlspecialchars($_POST['checkout-shipping-address']);

            $customerId = Customer::create($email, $title, $firstName, $lastName,
                $billingAddress, $shippingAddress, $lang['LANG']);

            $cartId = Cart::create($billingAddress, $shippingAddress, $customerId, $itemList);
            $cart = Cart::get($cartId);

            /* PayPal */
            include_once("controllers/processes/paypal-order.proc.php");
        }
    }

    displayAuthor();
    include_once('views/cart.view.php');
