<?php
    /**
     * Default home front controller
     * @author Antoine De Gieter
     */

    $user_id   = "1254592952";
    $client_id = "153a4cb9723e4b8da873fc4479ced431";
    $endpoint  = "https://api.instagram.com/v1/users/$user_id/media/recent/?client_id=$client_id";
    $curl = curl_init($endpoint);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $data = curl_exec($curl);
    $json = json_decode($data);
    $feed = Array();

    $iterator = 0;
    $stop = 4;
    foreach ($json->data as $post) {
        $feed[] = Array(
            "img" => $post->images->standard_resolution->url,
            "timestamp" => $post->created_time
        );
        if (++$iterator === $stop) break;
    }

    $images = array();
	foreach (glob("uploads/img/collections/*.jpg") as $image) {
		list($width, $height, $type, $attr) = getimagesize($image);
		$orientation = $width >= $height ? "landscape" : "portrait";
		$images[] = array(
			"src" => $image,
			"width" => $width,
			"height" => $height,
			"orientation" => $orientation
		);
	}

    $collections = array();
	foreach (glob("uploads/img/collections/*.jpg") as $image) {
		list($width, $height, $type, $attr) = getimagesize($image);
		$orientation = $width >= $height ? "landscape" : "portrait";
		$collections[] = array(
			"src" => $image,
			"width" => $width,
			"height" => $height,
			"orientation" => $orientation
		);
	}

    displayAuthor();
    include_once('views/enter.view.php');
