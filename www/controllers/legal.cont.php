<?php
	/**
	 * Legal notice front controller
	 * @author Antoine De Gieter
	 */
	displayAuthor();

	include_once('views/legal.view.php');
