<?php
    /**
     * Shop front controller
     * @author Antoine De Gieter
     */

    // Fetch items
    $filter = (isset($_GET['filter']) && in_array($_GET['filter'], array("tshirt", "sweatshirt", "skirt", "bag"))) ? $_GET['filter'] : "all";

    switch ($filter) {
        case 'all':
            $subcategories = Subcategory::getAll($lang['LANG']);
            break;

        default:
            $subcategories = Subcategory::get($filter, $lang['LANG']);
            break;
    }


    $itemsBySubcategory = Array();
    foreach ($subcategories as $subcategory) {
        $itemsBySubcategory[$subcategory['id']] = utf8_converter(Item::getBySubcategory($subcategory['id'], $lang['LANG']));
    }

    displayAuthor();
    include_once('views/shop.view.php');
