<?php
	/**
	 * Item front controller
	 * @author Antoine De Gieter
	 */
	displayAuthor();

    if (isset($_GET['item'])) {
        $itemId = htmlspecialchars($_GET['item']);
        if ($item = Item::get($itemId, $lang['LANG'])) {
        	include_once('views/item.view.php');
        } else {
            include_once('views/404.view.php');
        }
    } else {
        include_once('views/404.view.php');
    }
