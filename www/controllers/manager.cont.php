<?php
    /**
     * Default manager front controller
     * @author Antoine De Gieter
     */

    $ok = (isset($_COOKIE['MANAGER_PASSWORD_HASH'])
        && MANAGER_PASSWORD_HASH === $_COOKIE['MANAGER_PASSWORD_HASH'])
        || (isset($_POST['manager-password'])
        && MANAGER_PASSWORD_HASH === sha1($_POST['manager-password']));

    $error = isset($_POST['manager-password'])
        && MANAGER_PASSWORD_HASH !== sha1($_POST['manager-password']);

    $alert = "";

    if ($ok) {
        $stop = false;
        /* Cookies */
        setcookie("MANAGER_PASSWORD_HASH",
            MANAGER_PASSWORD_HASH,
            time() + (3600 * 24 * 7)); // store as cookie for a week

        /* Actions */
        if (isset($_GET['action'])) {
            switch ($_GET['object']) {
                case 'cart':
                    switch ($_GET['action']) {
                        case 'send':
                            $id = htmlspecialchars($_GET['id']);
                            Cart::updateState($id, "shipped");
                            $alert = "shipped";

                            /* email */
                            $cart = Cart::get($id);
                            $order = "";
                            $total = 0;
                            foreach ($cart['items'] as $item) {
                                $order .= "<tr>";
                                $order .= "<td>" . $item['quantity'] . " &times; " . $item['name'] . " (CHF " . $item['price'] . ".-)</td>";
                                $order .= "<td>";
                                if ("-" !== $item['size'])
                                    $order .= "<strong>" . $item['size'] . "</strong>";
                                else
                                    $order .= "";
                                $order .= "</td>";
                                $order .= "<td class=\"text-right\">";
                                $order .= "CHF " . ($item['price'] * $item['quantity']) . ".-";
                                $order .= "</td>";
                                $order .= "</tr>";
                                $total += $item['price'] * $item['quantity'];
                            }
                            $order .= "<tfoot>";
                            $order .= "<tr>";
                            $order .= "<th colspan=\"2\" class=\"text-right\">";

                            $order .= "Subtotal<br>";
                            $order .= "<span style=\"font-weight:normal;\">" . $lang['MAIL_SHIP']['shipping_handling'] . "</span><br>";
                            $order .= "<strong class=\"lead\">Total</strong>";

                            $order .= "</th>";

                            $order .= "<th class=\"text-right\">";

                            $order .= "CHF " . $total . ".-<br>";
                            $order .= "<span style=\"font-weight:normal;\">CHF 0.-</span><br>";
                            $order .= "<span class=\"lead\">CHF " . $total . ".-</span>";

                            $order .= "</th>";
                            $order .= "</tr>";
                            $order .= "</tfoot>";

                            $date = $lang['DAY_OF_THE_WEEK'][date('N', strtotime($cart['dt'])) - 1]
                                . " " . date('d', strtotime($cart['dt']))
                                . " " . $lang['MONTH'][date('n', strtotime($cart['dt'])) - 1]
                                . " " . date('Y', strtotime($cart['dt']));

                            $message = file_get_contents('global/html/templates/emails/order-shipment.templ.html');
                            $message = str_replace('%title%', $lang['MAIL_SHIP']['title'], $message);
                            $message = str_replace('%dear%', $lang['MAIL_SHIP']['dear'], $message);
                            $message = str_replace('%name%', $cart['c_name'], $message);
                            $message = str_replace('%message%', nl2p($lang['MAIL_SHIP']['message']), $message);
                            $message = str_replace('%order_n%', $lang['MAIL_SHIP']['order_n'], $message);
                            $message = str_replace('%order_date%', $date, $message);
                            $message = str_replace('%order_id%', $id, $message);
                            $message = str_replace('%shipping_address_title%', $lang['MAIL_SHIP']['shipping_address'], $message);
                            $message = str_replace('%shipping_address%', nl2p($cart['s_addr']), $message);
                            $message = str_replace('%billing_address_title%', $lang['MAIL_SHIP']['billing_address'], $message);
                            $message = str_replace('%billing_address%', nl2p($cart['b_addr']), $message);
                            $message = str_replace('%delivery_information%', $lang['MAIL_SHIP']['delivery_information'], $message);
                            $message = str_replace('%delivery_information_message%', nl2p($lang['MAIL_SHIP']['delivery_information_message']), $message);
                            $message = str_replace('%payment_method%', $lang['MAIL_SHIP']['payment_method'], $message);
                            $message = str_replace('%payment_method_message%', nl2p($lang['MAIL_SHIP']['payment_method_message']), $message);
                            $message = str_replace('%order_summary%', $lang['MAIL_SHIP']['order_summary'], $message);
                            $message = str_replace('%order%', $order, $message);
                            $message = str_replace('%switzerland%', $lang['MAIL_SHIP']['switzerland'], $message);

                            $mail = new PHPMailer();
                            $mail->IsSMTP(); // This is the SMTP mail server
                            // $mail->SMTPDebug = true;
                            // $mail->Debugoutput = 'echo';

                            $mail->SMTPSecure = 'tls';
                            $mail->Host = "smtp.mail.me.com";
                            $mail->Port = 587;
                            $mail->SMTPAuth = true;
                            $mail->Username = 'antoine.degieter@me.com';
                            $mail->Password = 'l4zy_C0mp4ny';

                            $mail->SetFrom('antoine.degieter@me.com');
                            $mail->AddAddress($cart['email']);
                            $mail->Subject = $lang['MAIL_SHIP']['title'] . " " . $id;
                            $mail->MsgHTML($message);
                            $mail->IsHTML(true);
                            $mail->CharSet="utf-8";
                            //$mail->AltBody(strip_tags($message));
                            if(!$mail->Send())
                                echo "Mailer Error: " . $mail->ErrorInfo;
                            break;

                        case 'cancel':
                            $id = htmlspecialchars($_GET['id']);
                            Cart::updateState($id, "cancelled");
                            $alert = "cancelled";
                            break;

                        case 'bill':
                            $id = htmlspecialchars($_GET['id']);
                            include_once('controllers/processes/generate-pdf-bill.proc.php');
                            $stop = true;
                            break;

                        default:

                            break;
                    }
                    break;

                case 'item':
                    switch ($_GET['action']) {
                        case 'create':
                            if (isset($_POST['add-item'])) {
                                /* Get infos */
                                $item_subcategory = $_POST['item-subcategory'];
                                $item_name = $_POST['item-name'];
                                $item_sales = isset($_POST['item-sales']);
                                $item_initial_price = $_POST['item-initial-price'];
                                $item_price = $_POST['item-price'];
                                $item_text_fr = $_POST['item-text-fr'];
                                $item_text_en = $_POST['item-text-en'];

                                /* Store into db */
                                $item_id = Item::create($item_name, $item_initial_price, $item_price,
                                    $item_subcategory, $item_sales, $item_text_fr, $item_text_en);

                                /* Process pictures */
                                $uploads_dir = 'uploads/img/items';
                                foreach ($_FILES["item-img"]["error"] as $key => $error) {
                                    if (UPLOAD_ERR_OK == $error) {
                                        $tmp_name = $_FILES["item-img"]["tmp_name"][$key];
                                        $img_id = 1 + $key;
                                        $name = "$item_id-$img_id.jpg";
                                        move_uploaded_file($tmp_name, "$uploads_dir/$name");
                                        Item::addPicture($item_id, $img_id);
                                    }
                                }
                                $alert = "created";
                            }
                            break;

                        case 'delete':
                            $id = htmlspecialchars($_GET['id']);
                            Item::delete($id);
                            foreach (glob("uploads/img/items/" . $id . "-*.jpg") as $filename) {
                               unlink($filename);
                            }
                            $alert = "deleted";
                            break;

                        default:

                            break;
                    }
                    break;

                default:

                    break;
            }
        }

        if (!$stop) {
            /* Carts */
            $carts = Cart::getAll();

            /* Items */
            $subcategories = Subcategory::getAll();

            $itemsBySubcategory = Array();
            foreach ($subcategories as $subcategory) {
                $itemsByCategory[$subcategory['id']] = array(
                    "subcategory" => $subcategory['name'],
                    "items" => utf8_converter(Item::getBySubcategory($subcategory['id'], $lang['LANG']))
                );
            }

            displayAuthor();
            include_once('views/manager.view.php');
        }
    } else {
        displayAuthor();
        include_once('views/password.view.php');
    }
