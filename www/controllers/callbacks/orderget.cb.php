<?php
    /**
     * Get order callback front controller
     * @author Antoine De Gieter
     */

    if (isSet($_GET['success']) && $_GET['success'] === 'true') {
        $cartId = $_GET['cart'];

        Cart::updateState($cartId, "paid");
        $cart = Cart::get($cartId);

        // Email Mélanie
        # TODO

        // Email client
        $order = "";
        $total = 0;
        foreach ($cart['items'] as $item) {
            $order .= "<tr>";
            $order .= "<td>" . $item['quantity'] . " &times; " . $item['name'] . " (CHF " . $item['price'] . ".-)</td>";
            $order .= "<td>";
            if ("-" !== $item['size'])
                $order .= "<strong>" . $item['size'] . "</strong>";
            else
                $order .= "";
            $order .= "</td>";
            $order .= "<td class=\"text-right\">";
            $order .= "CHF " . ($item['price'] * $item['quantity']) . ".-";
            $order .= "</td>";
            $order .= "</tr>";
            $total += $item['price'] * $item['quantity'];
        }
        $order .= "<tfoot>";
        $order .= "<tr>";
        $order .= "<th colspan=\"2\" class=\"text-right\">";

        $order .= "Subtotal<br>";
        $order .= "<span style=\"font-weight:normal;\">" . $lang['MAIL_ORDER']['shipping_handling'] . "</span><br>";
        $order .= "<strong class=\"lead\">Total</strong>";

        $order .= "</th>";

        $order .= "<th class=\"text-right\">";

        $order .= "CHF " . $total . ".-<br>";
        $order .= "<span style=\"font-weight:normal;\">CHF 0.-</span><br>";
        $order .= "<span class=\"lead\">CHF " . $total . ".-</span>";

        $order .= "</th>";
        $order .= "</tr>";
        $order .= "</tfoot>";

        $date = $lang['DAY_OF_THE_WEEK'][date('N', strtotime($cart['dt'])) - 1]
            . " " . date('d', strtotime($cart['dt']))
            . " " . $lang['MONTH'][date('n', strtotime($cart['dt'])) - 1]
            . " " . date('Y', strtotime($cart['dt']));

        $message = file_get_contents('global/html/templates/emails/order-shipment.templ.html');
        $message = str_replace('%title%', $lang['MAIL_ORDER']['title'], $message);
        $message = str_replace('%dear%', $lang['MAIL_ORDER']['dear'], $message);
        $message = str_replace('%name%', $cart['c_name'], $message);
        $message = str_replace('%message%', nl2p($lang['MAIL_ORDER']['message']), $message);
        $message = str_replace('%order_n%', $lang['MAIL_ORDER']['order_n'], $message);
        $message = str_replace('%order_date%', $date, $message);
        $message = str_replace('%order_id%', $cartId, $message);
        $message = str_replace('%shipping_address_title%', $lang['MAIL_ORDER']['shipping_address'], $message);
        $message = str_replace('%shipping_address%', nl2p($cart['s_addr']), $message);
        $message = str_replace('%billing_address_title%', $lang['MAIL_ORDER']['billing_address'], $message);
        $message = str_replace('%billing_address%', nl2p($cart['b_addr']), $message);
        $message = str_replace('%delivery_information%', $lang['MAIL_ORDER']['delivery_information'], $message);
        $message = str_replace('%delivery_information_message%', nl2p($lang['MAIL_ORDER']['delivery_information_message']), $message);
        $message = str_replace('%payment_method%', $lang['MAIL_ORDER']['payment_method'], $message);
        $message = str_replace('%payment_method_message%', nl2p($lang['MAIL_ORDER']['payment_method_message']), $message);
        $message = str_replace('%order_summary%', $lang['MAIL_ORDER']['order_summary'], $message);
        $message = str_replace('%order%', $order, $message);
        $message = str_replace('%switzerland%', $lang['MAIL_ORDER']['switzerland'], $message);

        $mail = new PHPMailer();
        $mail->IsSMTP(); // This is the SMTP mail server
        // $mail->SMTPDebug = true;
        // $mail->Debugoutput = 'echo';

        $mail->SMTPSecure = 'tls';
        $mail->Host = "smtp.mail.me.com";
        $mail->Port = 587;
        $mail->SMTPAuth = true;
        $mail->Username = 'antoine.degieter@me.com';
        $mail->Password = 'l4zy_C0mp4ny';

        $mail->SetFrom('antoine.degieter@me.com');
        $mail->AddAddress($cart['email']);
        $mail->Subject = $lang['MAIL_ORDER']['title'] . " " . $cartId;
        $mail->MsgHTML($message);
        $mail->IsHTML(true);
        $mail->CharSet="utf-8";
        //$mail->AltBody(strip_tags($message));
        if(!$mail->Send())
            echo "Mailer Error: " . $mail->ErrorInfo;

        displayAuthor();
        include_once('views/orderget_success.view.php');
    } else {
        $cartId = $_GET['cart'];

        Cart::updateState($cartId, "cancelled");

        $cart = Cart::get($cartId);

        $message = file_get_contents('global/html/templates/emails/order-cancellation.templ.html');
        $message = str_replace('%title%', $lang['MAIL_CANCEL']['title'], $message);
        $message = str_replace('%dear%', $lang['MAIL_CANCEL']['dear'], $message);
        $message = str_replace('%name%', $cart['c_name'], $message);
        $message = str_replace('%message%', nl2p($lang['MAIL_CANCEL']['message']), $message);
        $message = str_replace('%order_id%', $cartId, $message);
        $message = str_replace('%switzerland%', $lang['MAIL_CANCEL']['switzerland'], $message);

        $mail = new PHPMailer();
        $mail->IsSMTP(); // This is the SMTP mail server
        // $mail->SMTPDebug = true;
        // $mail->Debugoutput = 'echo';

        $mail->SMTPSecure = 'tls';
        $mail->Host = "smtp.mail.me.com";
        $mail->Port = 587;
        $mail->SMTPAuth = true;
        $mail->Username = 'antoine.degieter@me.com';
        $mail->Password = 'l4zy_C0mp4ny';

        $mail->SetFrom('antoine.degieter@me.com');
        $mail->AddAddress($cart['email']);
        $mail->Subject = $lang['MAIL_CANCEL']['title'] . " " . $cartId;
        $mail->MsgHTML($message);
        $mail->IsHTML(true);
        $mail->CharSet="utf-8";
        //$mail->AltBody(strip_tags($message));
        if(!$mail->Send())
            echo "Mailer Error: " . $mail->ErrorInfo;

        displayAuthor();
        include_once('views/orderget_failure.view.php');
    }
