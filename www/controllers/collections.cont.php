<?php
	/**
	 * Collections front controller
	 * @author Antoine De Gieter
	 */

	$images = array();
	foreach (glob("uploads/img/collections/*.jpg") as $image) {
		list($width, $height, $type, $attr) = getimagesize($image);
		$orientation = $width >= $height ? "landscape" : "portrait";
		$images[] = array(
			"src" => $image,
			"width" => $width,
			"height" => $height,
			"orientation" => $orientation
		);
	}
	
	displayAuthor();

	include_once('views/collections.view.php');
