<?php
	/**
	 * Contact front controller
	 * @author Antoine De Gieter
	 */
	displayAuthor();

	$condition = isset($_POST['contact-email']) && preg_match('/[a-zA-Z0-9-_.]+@[a-zA-Z0-9-_.]+\.[a-zA-Z0-9-_.]+/', $_POST['contact-email'])
		&& isset($_POST['contact-message']) && !empty($_POST['contact-message']);

	if ($condition) {
		include_once "lib/phpmailer.class.php";

		$email = htmlspecialchars($_POST['contact-email']);
		$message = htmlspecialchars($_POST['contact-message']);

		$mail = new PHPMailer();

     	$mail->From = 'no-reply@arrowstudio.ch';
		$mail->FromName = '[Contact] Arrow Studio';

		$mail->AddAddress("info@arrowstudio.ch");

		$mail->WordWrap = 50;
		$mail->IsHTML(true);

		$mail->Subject = utf8_decode("Message sur arrowstudio.ch");
		$mail->Body = '<h3>Arrow Studio</h3><h4>Message de <a href="mailto:' . $email . '">' . $email . '</a></h4><p>' . nl2br(utf8_decode($message)) . '</p>';

		if ($mail->Send())
			$sent = true;
	}

	include_once('views/contact.view.php');
