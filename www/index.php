<?php
	/**
	 * Default back controller
	 * @author Antoine De Gieter
	 */

	require_once 'config/config.inc.php';
	require_once 'lib/functions.inc.php';

	# Sessions
	session_save_path();
	session_start();

	# Database
	require_once 'lib/spdo.class.php';
	$dbh = SPDO::getInstance();/**/

	# PHPMailer
	require_once 'lib/phpmailer.class.php';/**/

	# Models
	function __autoload($model) {
		if (file_exists(DEFAULT_MODEL_PATH . strtolower($model) . DEFAULT_MODEL_EXTENSION))
			include_once(DEFAULT_MODEL_PATH . strtolower($model) . DEFAULT_MODEL_EXTENSION);
		else
			throw new Exception("Unable to load $model.");
	}

	# Set language
	if (isset($_GET['lang'])
	&& in_array($_GET['lang'], $authorized_languages)):
		$lang = $_GET['lang'];
		$_SESSION['lang'] = $lang;
		setcookie('lang', $lang, time() + (3600 * 24 * 30));
	elseif(isset($_SESSION['lang'])):
		$lang = $_SESSION['lang'];
	elseif(isset($_COOKIE['lang'])):
		$lang = $_COOKIE['lang'];
	else:
		$lang = DEFAULT_LANGUAGE;
	endif;

	include_once('language/' . $lang . '.lang.php');
	include_once('language/mail.' . $lang['LANG'] . '.lang.php');

	Subcategory::getAll();

	# Set page
	if (isset($_GET['page'])
	&& in_array($_GET['page'], $authorized_pages)):
		include_once('controllers/' . $_GET['page'] . '.cont.php');
	else:
		# Set callback
		if (isset($_GET['callback'])
		&& in_array($_GET['callback'], $authorized_callbacks)):
			include_once('controllers/callbacks/' . $_GET['callback'] . '.cb.php');
		else:
			include_once('controllers/enter.cont.php');
		endif;
	endif;
