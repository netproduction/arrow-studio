<?php
    /**
     * Default useful functions
     * @author Antoine De Gieter
     */

    # New line to paragraph
    function nl2p($string, $text_alignment = "justify") {
        if ($text_alignment != "justify"
        && $text_alignment != "center"
        && $text_alignment != "right"
        && $text_alignment != "left")
            $text_alignment = "justify";

        $paragraphs = '';

        foreach (explode("\n\n", $string) as $line):
            if (trim($line)):
                $paragraphs .=
                    '<p class="text-' . $text_alignment . '">' . $line . '</p>';
            endif;
        endforeach;

        $paragraphs = preg_replace("/[\n]-/", "\n&nbsp;&nbsp;&nbsp;&nbsp;&#9702;", $paragraphs);

        return preg_replace("/[\n]/", "<br>", $paragraphs);
    }

    # New line to title
    function nl2h($string, $level = "1", $text_alignment = "justify", $style="") {
        if ($text_alignment != "justify"
        && $text_alignment != "center"
        && $text_alignment != "right"
        && $text_alignment != "left")
            $text_alignment = "justify";

        if ($level < 1 || $level > 6)
            $level = 1;

        $paragraphs = '';

        foreach (explode("\n\n", $string) as $line):
            if (trim($line)):
                $paragraphs .=
                    '<h' . $level . ' class="text-' . $text_alignment . '" style="margin-top:0;' . $style . '">'
                    . $line . '</h' . $level . '>';
            endif;
        endforeach;

        return preg_replace("/[\n]/", "<br>", $paragraphs);
    }

    # Display author
    function displayAuthor() {
        echo '<!-- Designed by ' . DEFAULT_AUTHOR
            . ' (' . DEFAULT_COMPANY . ') -->';
    }

    # Convert array to UTF-8
    function utf8_converter($array) {
        array_walk_recursive($array, function(&$item, $key) {
            if(!mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_encode($item);
            }
        });

        return $array;
    }

    # Get current URL
    function url(){
        return sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['REQUEST_URI']
        );
    }
