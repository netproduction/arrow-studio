$(document).ready(function() {

	//new slimScroll($('#middle-box')[0]);

	MIDDLE_BOX_SCROLL_AMOUNT = $('.middle-box').height();
	if (MIDDLE_BOX_SCROLL_AMOUNT == null)
		MIDDLE_BOX_SCROLL_AMOUNT = $('.alternate-middle-box').height();

	// Enter page scroll
	// $("#enter_arrow").click(function(){
	// 	$("#section_enter_after").css("height", "100%");
	// 	$("#fullpage").animate({
	// 		scrollTop: $("#universe_anchor").offset().top
	// 	}, 800, function () {
	// 		$("#section_enter").remove();
	// 		$("#section_enter_after").css("top", "0");
	// 	});
	// 	return false;
	// });

	$('#fullpage').fullpage({
        //Navigation
        menu: false,
        lockAnchors: false,
        anchors:['collections'],
        navigation: false,
        navigationPosition: 'right',
        showActiveTooltip: false,
        slidesNavigation: true,
        slidesNavPosition: 'bottom',

        //Scrolling
        css3: true,
        scrollingSpeed: 700,
        autoScrolling: true,
        fitToSection: true,
        fitToSectionDelay: 1000,
        scrollBar: false,
        easing: 'easeInOutCubic',
        easingcss3: 'ease',
        loopBottom: false,
        loopTop: false,
        loopHorizontal: true,
        continuousVertical: false,
        normalScrollElements: '#element1, .element2',
        scrollOverflow: false,
        touchSensitivity: 15,
        normalScrollElementTouchThreshold: 5,

        //Accessibility
        keyboardScrolling: true,
        animateAnchor: true,
        recordHistory: true,

        //Design
        controlArrows: true,
        verticalCentered: true,
        resize : false,
        sectionsColor : ['#fff', '#fff'],
        paddingTop: '0px',
        paddingBottom: '0px',
        fixedElements: '#header',
        responsiveWidth: 0,
        responsiveHeight: 0,

        //events
        onLeave: function(index, nextIndex, direction){
			if(nextIndex == 1) {
                return false;
            }
		},
        afterLoad: function(anchorLink, index){
			if (index === 2) {
				//$.fn.fullpage.destroy();
				//$("#enter").remove();
			}
		},
        afterRender: function(){

		},
        afterResize: function(){

		},
    });

	// Cart initialization
	Cart.get();
	refreshCart();

	$(".add-item").on('click', function(event) {
	    Cart.addItem($(this).data('item-id'), $(this).data('item-name'), $(this).data('item-price'), $("input:radio[name='size-radio']:checked").val());
	    refreshCart();
	});

	$(".empty").on('click', function(event) {
	    var ok = confirm("");
	    if (ok) {
	        Cart.init();
	        refreshCart();
	    }
	});

    $("#middle-box").height($(window).height() - 130);
    $("#enter-middle-box").height($(window).height() - 130);
	if ($(window).width() > 991)
    	$("#middle-box").height($(window).height() - 210);
    $("#enter-middle-box").height($(window).height() - 200);
    $("#alternate-middle-box").height($(window).height() - 200);
    $("#big-picture").height($(window).height() - 200);
    $("#form-add-to-cart").css("top", $(window).height() - 370 + "px");


	$(".instagram-img").width($(window).height() / 3);
	if ($(window).height() > 1080) {
		$(".instagram-img").width($(window).height() / 4);
		$("#enter-middle-box").css("paddingTop", ($(window).height() / 5) + "px");
	}

	// Lightbox initialization
	lightbox.init();
});

$(window).on("resize", function() {
    $("#middle-box").height($(window).height() - 200);
	if ($(window).width() > 991)
    	$("#middle-box").height($(window).height() - 280);
    $("#enter-middle-box").height($(window).height() - 200);
    $("#alternate-middle-box").height($(window).height() - 200);
    $("#big-picture").height($(window).height() - 200);
    $("#form-add-to-cart").css("top", $(window).height() - 300 + "px");

	$(".instagram-img").width($(window).height() / 3);
	if ($(window).height() > 1080)
		$(".instagram-img").width($(window).height() / 4);
});
