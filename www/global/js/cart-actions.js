function refreshCart() {
    var cart = Cart.get();
    $('.cart-count').html(Cart.count());
    var contents = '';
    $('#cart-contents').html(contents);
    $('#checkout-cart-contents').html(contents);
    for (var index in cart.items) {
        $('#cart-contents').append('<tr>');

        $('#cart-contents').append('<td class="text-left"><a href="?page=item&item=' + cart.items[index].id + '" class="link-black"><img src="uploads/img/items/' + cart.items[index].id + '-1.jpg" style="margin-bottom:2px;" width="180px"></a></td>');

        if (cart.items[index].itemsize == undefined)
            $('#cart-contents').append('<td class="text-left"><a href="?page=item&item=' + cart.items[index].id + '" class="link-black">' + cart.items[index].name + '</a></td>');
        else
            $('#cart-contents').append('<td class="text-left"><a href="?page=item&item=' + cart.items[index].id + '" class="link-black">' + cart.items[index].name + '</a><br><br>Size: <strong>' + cart.items[index].itemsize + '</strong></td>');

        $('#cart-contents').append('<td class="text-center">CHF ' + cart.items[index].price + '.-</td>');

        $('#cart-contents').append('<td class="text-center">' + cart.items[index].quantity + '</td>')

        /*button class="btn btn-link link-black minus-item" data-item-id="' + cart.items[index].id + '" data-item-size="' + cart.items[index].size + '"><span class="glyphicon glyphicon-minus" data-item-id="' + cart.items[index].id + '" data-item-size="' + cart.items[index].size + '"></span></button><button class="btn btn-link link-black plus-item" data-item-id="' + cart.items[index].id + '" data-item-size="' + cart.items[index].size + '"><span class="glyphicon glyphicon-plus" data-item-id="' + cart.items[index].id + '" data-item-size="' + cart.items[index].size + '"></span></button>*/

        $('#cart-contents').append('<td class="text-right">CHF ' + (parseInt(cart.items[index].quantity) * parseFloat(cart.items[index].price)) + '.-</td>');

        $('#cart-contents').append('<td class="text-right"><button class="btn btn-link link-black remove-item" data-item-id="' + cart.items[index].id + '" data-item-size="' + cart.items[index].size + '"><span data-item-id="' + cart.items[index].id + '" data-item-size="' + cart.items[index].size + '">&times;</span></button></td>');

        $('#cart-contents').append('</tr>');
    }

    $('#checkout-cart-contents').html(contents);
    $('#cart-total').html(Cart.total());
    $('#checkout-cart-total').html(Cart.total());
    $(".remove-item").on('click', function(event) {
        var ok = confirm("?");
        if (ok) {
            Cart.removeItem($(this).data('item-id'), $(this).data('item-size'), refreshCart);
        }
    });
}

function launchFakeLoader() {

    $("#fakeloader").fakeLoader({
        timeToHide: 100000,
        zIndex: 999,
        spinner: "spinner1",
        bgColor: "rgba(0,0,0,0.5)",
    });
}
