Cart = {
    get: function() {
        var cart = Cookies.getJSON('cart');
        if (undefined === cart) {
            this.init();
        }
        return cart;
    },

    set: function(cart) {
        Cookies.set('cart', cart, { expires: 7 });
    },

    count: function() {
        var cart = this.get();
        var count = 0;
        for (var index in cart.items) {
            count += cart.items[index].quantity;
        }
        return count;
    },

    total: function() {
        var cart = this.get();
        var total = 0;
        for (var index in cart.items) {
            total += parseInt(cart.items[index].quantity) * parseFloat(cart.items[index].price);
        }
        return total;
    },

    addItem: function(item, name, price, size) {
        console.log(item, name, price, size);
        var cart = this.get();
        var isPresent = false;
        for (var index in cart.items) {
            if (cart.items[index].id === item && cart.items[index].size == size) {
                cart.items[index].quantity++;
                isPresent = true;
                break;
            }
        }
        if (!isPresent)
            cart.items.push({
                "id": item,
                "name": name,
                "price": price,
                "itemsize": size,
                "quantity": 1
            });
        this.set(cart);
    },

    removeItem: function(item, size, callback) {
        console.log("Removing item");
        var cart = this.get();
        var isPresent = false;
        for (var index in cart.items) {
            if (cart.items[index].id == item && cart.items[index].size == size) {
                if (cart.items[index].quantity <= 1) {
                    cart.items.splice(index, 1);
                    break;
                } else {
                    cart.items[index].quantity--;
                    break;
                }
            }
        }
        this.set(cart);
        callback();
    },

    setEmail: function(email) {
        var cart = this.get();
        cart.email = email;
        this.set(cart);
    },

    init: function() {
        // Generates a 40-digit random token
        var token = Math.random().toString(16).substr(2)
            + Math.random().toString(16).substr(2)
            + Math.random().toString(16).substr(2)
            + Math.random().toString(16).substr(2)
            + new Date().getTime().toString(16).substr(3);

        cart = {
            "token": token,
            "items": []
        };
        this.set(cart);
    },

    send: function() {
        var cart = this.get();
    }
};
