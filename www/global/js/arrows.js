$(document).ready(function() {
    // Middle box arrow
    if ($("#middle-box").length) {
        $container = $("<div></div>");
        $container.addClass("middle-box-arrow");
        $container.insertAfter("#middle-box");
        $up = $("<div>&#9650;</div>");
        $up.attr("title", "scroll up");
        $up.appendTo($container);
        $up.click(function () {
            $("#middle-box").animate({
                scrollTop: $("#middle-box").scrollTop() - MIDDLE_BOX_SCROLL_AMOUNT
            });
        });
        $down = $("<div>&#9660;</div>");
        $down.attr("title", "scroll down");
        $down.appendTo($container);
        $down.click(function () {
            $("#middle-box").animate({
                scrollTop: $("#middle-box").scrollTop() + MIDDLE_BOX_SCROLL_AMOUNT
            });
        });
    }

    if ($(".alternate-middle-box").length) {
        $container = $("<div></div>");
        $container.addClass("middle-box-arrow");
        $container.insertAfter(".alternate-middle-box");
        $up = $("<div>&#9650;</div>");
        $up.attr("title", "scroll up");
        $up.appendTo($container);
        $up.click(function () {
            $(".alternate-middle-box").animate({
                scrollTop: $(".alternate-middle-box").scrollTop() - MIDDLE_BOX_SCROLL_AMOUNT
            });
        });
        $down = $("<div>&#9660;</div>");
        $down.attr("title", "scroll down");
        $down.appendTo($container);
        $down.click(function () {
            $(".alternate-middle-box").animate({
                scrollTop: $(".alternate-middle-box").scrollTop() + MIDDLE_BOX_SCROLL_AMOUNT
            });
        });
    }
});
