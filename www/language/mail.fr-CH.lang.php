<?php
    /**
     * French language file for mails
     * @author Antoine De Gieter
     */

    $lang['MAIL_ORDER'] = array(
        "title" => "Confirmation de commande",
        "dear" => "Cher(e)",
        "message" => "Merci pour vos achats sur <a href=\"http://www.arrowstudio.ch\">arrowstudio.ch</a>. Veuillez noter que cet e-mail ne fait que confirmer que nous avons reçu votre commande correctement. Une fois que votre commande sera traitée et votre colis sera expédié, nous vous enverrons un nouvel email de confirmation de commande définitive.\r\n\r\nCordialement,\r\nArrow Studio",
        "shipping_address" => "Adresse de livraison",
        "billing_address" => "Adresse de facturation",
        "delivery_information" => "Informations sur la livraison",
        "delivery_information_message" => "Commande livrée par – La Poste Suisse –\r\nLivraison sous 3-5 jours ouvrables",
        "payment_method" => "Méthode de paiement",
        "payment_method_message" => "PayPal",
        "order_n" => "Commande n°",
        "order_date" => "Date",
        "order_summary" => "Récapitulatif de la commande",
        "subtotal" => "Sous-total",
        "shipping_handling" => "Expédition & traitement",
        "total" => "Total",
        "tax" => "T.V.A.",
        "switzerland" => "Suisse"
    );
    /*$lang['MAIL_PURCHASE'] = array(
        "title" => "Payment confirmation",
        "dear" => "Dear",
        "message" => "",
        "shipping_address" => "Adresse de livraison",
        "billing_address" => "Adresse de facturation",
        "delivery_information" => "Informations sur la livraison",
        "delivery_information_message" => "",
        "payment_method" => "Méthode de paiement",
        "payment_method_message" => "Méthode de paiement : PayPal",
        "order_n" => "Commande n°",
        "order_date" => "Date",
        "order_summary" => "Récapitulatid de la commande",
        "subtotal" => "Sous-total",
        "shipping_handling" => "Expédition & Traitement",
        "total" => "Total",
        "tax" => "T.V.A.",
        "switzerland" => "Suisse"
    );*/
    $lang['MAIL_CANCEL'] = array(
        "title" => "Paiement Paypal non reçu",
        "dear" => "Cher(e)",
        "message" => "Nous regrettons de vous informer que votre commande %order_id% a été annulée, car nous n'avons pas reçu votre paiement Paypal pour votre achat.\r\n\r\nNous serions heureux de vous aider à compléter votre commande. Veuillez nous écrire à <a href=\"mailto:order@arrowstudio.ch\">orders@arrowstudio.ch</a>\r\n\r\nNous vous remercions de votre commande chez Arrow Studio.\r\n\r\nCordialement,\r\n\r\nService clientèle Arrow Studio",
        "order_n" => "Commande n°",
        "order_date" => "Date",
        "switzerland" => "Suisse"
    );
    $lang['MAIL_SHIP'] = array(
        "title" => "Confirmation d'envoi",
        "dear" => "Cher(e)",
        "message" => "La commande %order_id% passée le %order_date% vient d'être envoyée. Votre numéro de tracking suivra sous peu dans un prochain mail.\r\n\r\nNous vous remercions de votre commande chez Arrow Studio.\r\n\rCordialement,\r\nService clientèle Arrow Studio",
        "shipping_address" => "Adresse de livraison",
        "billing_address" => "Adresse de facturation",
        "delivery_information" => "Informations sur la livraison",
        "delivery_information_message" => "Commande livrée par – La Poste Suisse –\r\nLivraison sous 3-5 jours ouvrables",
        "payment_method" => "Méthode de paiement",
        "payment_method_message" => "PayPal",
        "order_n" => "Commande n°",
        "order_date" => "Date",
        "order_summary" => "Récapitulatif de la commande",
        "subtotal" => "Sous-total",
        "shipping_handling" => "Expédition & Traitement",
        "total" => "Total",
        "tax" => "T.V.A.",
        "switzerland" => "Suisse"
    );
    $lang['INVOICE'] = array(
        "invoice_n" => "Invoice n°",
        "invoice_date" => "Invoice date",
        "order_n" => "Commande n°",
        "sold_to"=> "Sold to",
        "shipped_to" => "Shipped to",
        "item" => "Item",
        "quantity" => "Quantity",
        "size" => "Size",
        "price" => "Item price",
        "discount" => "Discount",
        "subtotal" => "Sous-total",
        "delivery_charges" => "Delivery charges",
        "total" => "Total",
        "payment_method" => "Méthode de paiement",
        "switzerland" => "Suisse"
    );
