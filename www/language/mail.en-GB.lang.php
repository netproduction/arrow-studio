<?php
    /**
     * English language file for mails
     * @author Antoine De Gieter
     */

    $lang['MAIL_ORDER'] = array(
        "title" => "Order confirmation",
        "dear" => "Dear",
        "message" => "Thank you for shopping on <a href=\"http://www.arrowstudio.ch\">arrowstudio.ch</a>. Please note that this email only confirms that we have received your order correctly. Once your order is processed and your package ships we will send an email with your final order.\r\n\r\nKind regards,\r\nArrow Studio",
        "shipping_address" => "Shipping address",
        "billing_address" => "Billing address",
        "delivery_information" => "Delivery information",
        "delivery_information_message" => "Order delivered with – Swiss Post –\r\nDelivery within 3-5 business days",
        "payment_method" => "Payment method",
        "payment_method_message" => "PayPal",
        "order_n" => "Order no.",
        "order_date" => "Date",
        "order_summary" => "Order summary",
        "subtotal" => "Subtotal",
        "shipping_handling" => "Shipping &amp; handling",
        "total" => "Total (Incl. Tax)",
        "tax" => "Tax",
        "switzerland" => "Switzerland"
    );
    /*$lang['MAIL_PURCHASE'] = array(
        "title" => "Payment confirmation",
        "dear" => "Dear",
        "message" => "",
        "shipping_address" => "Shipping address",
        "billing_address" => "Billing address",
        "delivery_information" => "Delivery information",
        "delivery_information_message" => "",
        "payment_method" => "Payment method",
        "payment_method_message" => "Payment method: PayPal",
        "order_n" => "Order no.",
        "order_date" => "Date",
        "order_summary" => "Order summary",
        "subtotal" => "Subtotal",
        "shipping_handling" => "Shipping &amp; handling",
        "total" => "Total (Incl. Tax)",
        "tax" => "Tax",
        "switzerland" => "Switzerland"
    );*/
    $lang['MAIL_CANCEL'] = array(
        "title" => "Paypal Payment not Received",
        "dear" => "Dear",
        "message" => "We regret to inform you that your order %order_id% has been cancelled, as we did not receive your Paypal payment for your purchase.\r\n\r\nWe would be happy to assist you in completing your order. Please email us at <a href=\"mailto:orders@arrowstudio.ch\">orders@arrowstudio.ch</a>\r\n\r\nMany thanks for shopping at Arrow Studio.\r\n\r\nSincerely,\r\nArrow Studio Customer Service",
        "order_n" => "Order no.",
        "order_date" => "Date",
        "switzerland" => "Switzerland"
    );
    $lang['MAIL_SHIP'] = array(
        "title" => "Shipping confirmation",
        "dear" => "Dear",
        "message" => "The order %order_id% you placed on the %order_date% has just been sent. Your tracking number will follow shortly in an upcoming email.\r\nYou may track the shipping of your package no xxxx here.\r\nPlease be aware that, while your shipment has been processed and dispatched, it will take several hours before your number will be registered by our shipping partner.\r\n\r\nMany thanks for shopping at Arrow Studio.\r\n\r\nSincerely,\r\nArrow Studio Customer Service",
        "shipping_address" => "Shipping address",
        "billing_address" => "Billing address",
        "delivery_information" => "Delivery information",
        "delivery_information_message" => "Order delivered with – Swiss Post –\r\nDelivery within 3-5 business days",
        "payment_method" => "Payment method",
        "payment_method_message" => "PayPal",
        "order_n" => "Order no.",
        "order_date" => "Date",
        "order_summary" => "Order summary",
        "subtotal" => "Subtotal",
        "shipping_handling" => "Shipping &amp; handling",
        "total" => "Total (Incl. Tax)",
        "tax" => "Tax",
        "switzerland" => "Switzerland"
    );
    $lang['INVOICE'] = array(
        "invoice_n" => "Invoice no.",
        "invoice_date" => "Invoice date",
        "order_n" => "Order no.",
        "sold_to"=> "Sold to",
        "shipped_to" => "Shipped to",
        "item" => "Item",
        "quantity" => "Quantity",
        "size" => "Size",
        "price" => "Item price",
        "discount" => "Discount",
        "subtotal" => "Subtotal",
        "delivery_charges" => "Delivery charges",
        "total" => "Total",
        "payment_method" => "Payment method",
        "switzerland" => "Switzerland"
    );
