<?php
    /**
     * French language file
     * @author Antoine De Gieter
     */

    $lang = Array();
    $lang['LANG'] = "fr-CH";

    /* DATE */
    $lang['DAY_OF_THE_WEEK'] = array(
        "Lundi",
        "Mardi",
        "Mercredi",
        "Jeudi",
        "Vendredi",
        "Samedi",
        "Dimanche",
    );

    $lang['MONTH'] = array(
        "Janvier",
        "Février",
        "Mars",
        "Avril",
        "Mai",
        "Juin",
        "Juillet",
        "Août",
        "Septembre",
        "Octobre",
        "Novembre",
        "Décembre",
    );

	/* BUTTONS */
	$lang['BUTTON_DOWNLOAD'] = "Télécharger";

    /* MENU */
    $lang['MENU_COLLECTIONS'] = "Collections";
    $lang['MENU_SHOP'] = "E-Boutique";
	$lang['MENU_SHOP_PAP'] = "Prêt-à-porter";
	$lang['MENU_SHOP_ACC'] = "Accessoires";
    $lang['MENU_SHOP_TSHIRT'] = "T-Shirt";
    $lang['MENU_SHOP_SWEATSHIRT'] = "Sweatshirt";
    $lang['MENU_SHOP_SKIRT'] = "Jupe";
    $lang['MENU_SHOP_BAG'] = "Sac";
    $lang['MENU_UNIVERSE'] = "Univers";
    $lang['MENU_ABOUT'] = "À propos";
	$lang['MENU_PHILOSOPHY'] = "Philosophie";
	$lang['MENU_DESIGNER'] = "Designer";
    $lang['MENU_BLOG'] = "Blog";
    $lang['MENU_CONTACT'] = "Contact";
    $lang['MENU_CART'] = "Panier";

    /* FOOTER */
    $lang['FOOTER_LEGAL_NOTICE'] = "Mention légales";
    $lang['FOOTER_PRESS'] = "Presse";
    $lang['FOOTER_CREDITS'] = "Crédits";

    /* SHOP */
    $lang['SHOP_ADD_TO_CART'] = "Ajouter au panier";

    /* CART */
    $lang['CART_CONTENTS'] = "Contenu du panier";
    $lang['CART_CHECKOUT'] = "Valider ce panier";
    $lang['CART_EMPTY'] = "Vider le panier";
    $lang['CART_TOTAL'] = "Total";
    $lang['CART_CONFIRM_EMPTY'] = "Voulez-vous vraiment vider le panier ?";
    $lang['CART_CONFIRM_REMOVE_ITEM'] = "Voulez-vous vraiment supprimer cet élément ?";
    $lang['CART_BACK_TO_SHOP'] = "Retour à la boutique";

    /* CHECKOUT */
    $lang['CHECKOUT_TITLE'] = "Valider le panier";
    $lang['CHECKOUT_FIRST_NAME'] = "Prénom";
    $lang['CHECKOUT_LAST_NAME'] = "Nom";
    $lang['CHECKOUT_EMAIL'] = "Adresse électronique";
    $lang['CHECKOUT_TITLES'] = array(
        "M", "Mme", "Mle"
    );
    $lang['CHECKOUT_BILLING_ADDRESS'] = "Adresse de facturation";
    $lang['CHECKOUT_SHIPPING_ADDRESS'] = "Adresse de livraison";
    $lang['CHECKOUT_PROCEED'] = "Procéder au paiement";
    $lang['CHECKOUT_COPY'] = "Copier";

    /* ABOUT */
    $lang['PHILOSOPHY_TITLE'] = "Philosophie";
    $lang['PHILOSOPHY_TEXT'] = "Arrow Studio voudrait voir ralentir cet excès de consommation dans lequel nous vivons.\r\n\r\nLes saisons s'accélèrent et les modes se propagent de plus en plus vite, ce qui conduit à davantage de consommation et de déchets. Nous savons qu'il est parfois difficile de prendre conscience de cet état des choses et de ses conséquences. C'est pourquoi il nous est important de créer des vêtements intemporels et de qualité.\r\n\r\nAvoir une production française, grâce à laquelle nous pouvons allier proximité de la production, qualité et savoir-faire, est indispensable.";
    $lang['DESIGNER_TITLE'] = "Mélanie Begg";
    $lang['DESIGNER_TEXT'] = "Arrow Studio est la création de Mélanie Begg, designer helvético-britannique, qui fonda la marque à Lausanne (Suisse) en 2013 afin d'exprimer une vision de la mode bien précise.\r\n\r\nDans son travail artistique, Mélanie tire son inspiration du quotidien, de la rue, de la musique, du design, de l'architecture, et d'un style de vie minimaliste.";
    $lang['ABOUT_TITLE'] = "À propos";
    $lang['ABOUT_TEXT'] = "Arrow Studio est une marque de vêtements féminins fondée à Lausanne (Suisse) en 2013." . PHP_EOL . PHP_EOL
        . "Le style Arrow Studio se définit dans un minimalisme mesuré, mais affirmé, dans un « effortless spirit » qui fait la part belle aux coupes brutes et aux tissus de haute qualité. Les matières sont en effet sélectionnées avec le plus grand soin et les vêtements fabriqués en France, dans l’idée d’obtenir le meilleur équilibre entre respect de l’environnement, qualité et savoir-faire." . PHP_EOL . PHP_EOL
        . "On pourra retrouver dans les pièces de chez Arrow Studio une prédominance des noirs et des blancs, combinés dans un look empreint de sobriété, anonyme et discret. Des pièces essentielles, accessibles à tout âge et qui répondront aux demandes d’une femme résolument moderne." . PHP_EOL . PHP_EOL
        . "La directrice artistique Mélanie Begg souhaite créer des pièces marquées par une intemporalité trouvera sa source dans la simplicité des couleurs et la pureté des lignes, tandis que les matières utilisées permettront aux modèles d’être portés en toute saison. Le style Arrow Studio vise donc à défier le temps, tant dans son aspect temporel que saisonnier.";


    /* MANAGER */
    $lang['MANAGER_TITLE'] = "Espace de gestion";
    $lang['MANAGER_RESTRICTED_ACCESS'] = "Acc&egrave;s restreint";
    $lang['MANAGER_PASSWORD'] = "Mot de passe";
    $lang['MANAGER_SUBMIT'] = "Soumettre";
    $lang['MANAGER_CARTS'] = "Commandes";
    $lang['MANAGER_ITEMS'] = "Articles";

    /* UNIVERSE */
	$lang['UNIVERSE_TITLE'] = "Notre univers défini en quelques mots";
	$lang['UNIVERSE_DEFINITIONS'] = array(
		"<strong>Basique, n. m.</strong> Vêtement essentiel pour la vie de tous les jours.",
		"<strong>Effortless, adj. (adj. angl.).</strong> Qualifie un style qui ne nécessite rien de superflu.",
		"<strong>Garçonne, n. f.</strong> Fille ayant les allures d’un garçon.",
		"<strong>Intemporel, adj.</strong> Qui est indépendant du temps, qui ne varie pas avec lui ; indémodable.",
		"<strong>Minimalisme, n. m.</strong> <strong>1.</strong> Style artistique et esthétique qui met en valeur les lignes et les formes épurées, simples. <strong>2.</strong> Style de vie, philosophie qui place la simplicité comme valeur essentielle."
	);

    /* CREDITS */
	$lang['CREDITS_TITLE'] = "Credits";
	$lang['CREDITS_CONTENTS'] = array(
		array(
			"title" => "Photographe",
			"people" => array(
				array(
					"name" => "Clément Ardin",
					"url" => "http://a-c-portfolio.tumblr.com",
				),
				array(
					"name" => "Matthieu Spohn",
					"url" => "http://www.matthieu-spohn.com",
				),
			)
		),
        array(
            "title" => "Graphiste",
            "people" => array(
                array(
                    "name" => "Céline Tissot",
                    "url" => "http://celinetissot.ch"
                )
            )
        ),
		array(
			"title" => "Mannequin",
			"people" => array(
				array(
					"name" => "Anna Noélia Garcia",
					"url" => "https://www.facebook.com/annanoelia.garcia",
				),
				array(
					"name" => "Sandy Vaz",
					"url" => "",
				),
				array(
					"name" => "Joelle Catin",
					"url" => "",
				),
			)
		),
		array(
			"title" => "Web",
			"people" => array(
				array(
					"name" => "Net Production",
					"url" => "http://www.net-production.ch",
				),
			)
		)
	);

	/* LEGAL NOTICE */
	$lang['LEGAL_NOTICE_TEXT'] = "<strong>Bienvenue sur notre site web <a href=\"www.arrowstudio.ch\" class=\"link-black\">arrowstudio.ch</a></strong>.\r\n
    \r\n
    Ces conditions générales d'utilisation sont régies par le droit suisse et en particulier par le \"Code des Obligations Suisses\". L'accès et l'utilisation de ce site web ainsi que l'achat de produits sur arrowstudio.ch sont fondés sur l'hypothèse que ces conditions générales d'utilisation aient été lues, comprises et acceptées par vous.\r\n
    \r\n
    Pour de plus amples demandes d'information: <a href=\"mailto:info@arrowstudio.ch\" class=\"link-black\">info@arrowstudio.ch</a>.\r\n
    \r\n
    Arrow Studio peut modifier ou simplement mettre à jour ces conditions générales d'utilisation. Par conséquent, vous devriez consulter régulièrement cette section sur le site web afin de vérifier la publication des plus récentes mises à jour et conditions d'utilisation de arrowsutio.ch. Si vous n'êtes pas d'accord sur tout ou partie de ces  conditions d'utilisation du site arrowstudio.ch, n'utilisez donc pas notre site Web.\r\n
    \r\n
    L'accès et l'utilisation de arrowstudio.ch, y compris l'affichage des pages web, la communication avec Arrow Studio, le téléchargement d'informations de produits et les achats sur le site web, sont effectuées par nos utilisateurs exclusivement à des fins personnelles, et devrait en aucune façon être connecté à tout commerce, activité commerciale ou professionnelle. Rappelez-vous que vous serez responsable de votre utilisation de arrowstudio.ch et son contenu. Arrow Studio ne doit pas être considéré comme responsable de toute utilisation du site web et de son contenu fait par ses utilisateurs qui ne sont pas conformes aux lois et règlements en vigueur, sans préjudice de la responsabilité du fournisseur pour les délits intentionnels et de négligence grave.\r\n
    \r\n
    En particulier, vous serez responsable de la communication d'informations ou de données qui ne sont pas correctes, fausses ou concernant des tiers (dans le cas où ces tiers n'ont pas donné leur consentement) ainsi que pour toute utilisation abusive de ces données ou informations.\r\n
    \r\n
    \r\n
    <strong>1.Politique de Confidentialité </strong>\r\n
    \r\n
    Nous vous recommandons de lire la politique de confidentialité qui vaut également dans le cas où les utilisateurs accèdent à arrowstudio.ch et utiliser les services concernés sans faire des achats. La politique de confidentialité vous aidera à comprendre comment et à quelles fins arrowstudio.ch collecte et utilise vos données personnelles.\r\n
    \r\n
    \r\n
    <strong>2. Droits de propriété intellectuelle</strong>\r\n
    \r\n
    Tout le contenu de ce site, tels que les œuvres, images, photos, dialogues, musiques, sons, vidéos, documents, les dessins, les logos, les menus, les pages web, les graphiques, les couleurs, les plans, les outils, les polices, les dessins, les diagrammes, les dispositions, les méthodes, les processus, les fonctions et les logiciels (collectivement appelé \"contenu\"), est la propriété de Arrow Studio Lausanne et est protégé par le droit d'auteur national et international et d'autres lois de propriété intellectuelle. Vous ne pouvez pas reproduire, publier, distribuer, afficher, modifier, créer des œuvres dérivées, ou exploiter de quelque manière, tout ou partie, du contenu sans le consentement exprès préalable écrit de Arrow Studio Lausanne.\r\n
    Arrow Studio Lausanne a le droit exclusif d'autoriser ou d'interdire, à sa seule discrétion, toute reproduction, publication, distribution, affichage, modification, création d'œuvres dérivées, ou exploitation de n'importe quelle manière, de tout ou partie du contenu. Arrow Studio Lausanne a le droit, à tout moment, de revendiquer la paternité de tout contenu affiché sur ce site et de s'opposer à toute utilisation, déformation ou autre modification d'un tel contenu.\r\n
    Toute reproduction, publication, distribution, affichage, modification, création de travaux dérivés à partir du contenu et expressément autorisée par écrit par Arrow Studio Lausanne doit être effectuée par vous, et vous seul et en conformité avec les lois applicables.\r\n
    \r\n
    \r\n
    <strong>3. Liens vers d'autres sites Web</strong>\r\n
    \r\n
    arrowstudio.ch peut contenir des liens vers d'autres sites web qui ne sont en aucune façon liés à arrowstudio.ch. Arrow Studio n'a pas à contrôler ou surveiller ces sites Web de tiers ou leur contenu.  Il ne pourra être tenu pour responsable du contenu de ces sites et/ou pour les règles adoptées par elles en ce qui concerne, mais sans s'y limiter, votre vie privée et le traitement de vos données personnelles lorsque vous visitez ces sites web. Veuillez, faire attention lorsque vous accédez à ces sites web à travers les liens fournis sur arrowstudio.ch et lisez attentivement les termes et conditions d'utilisation et leurs politiques de confidentialité. Nos Conditions générales d'utilisation et la politique de confidentialité ne sont pas applicables aux sites web de tiers. arrowstudio.ch fournit des liens vers d'autres sites web exclusivement pour aider ses utilisateurs dans la recherche et la navigation sur Internet et de permettre des liens vers des sites web. Lorsque Arrow Studio fournit des liens vers d'autres sites web, Arrow Studio ne recommande pas à ses utilisateurs d'accéder à ces sites web et il ne fournit pas de garanties à leur contenu web ou à des services et produits fournis et vendus par ces sites web aux utilisateurs d'internet.\r\n
    \r\n
    \r\n
    <strong>4. Liens vers arrowstudio.ch</strong>\r\n
    \r\n
    Veuillez, contacter Arrow Studio à l'adresse e-mail suivante: info@arrowstudio.ch si vous êtes intéressé à lier la page d'accueil de arrowstudio.ch et d'autres pages web qui peuvent être accessibles au public. Vous êtes priés de contacter Arrow Studio pour demander notre consentement à relier arrowstudio.ch. L'hébergeur garanti le lien vers arrowstudio.ch gratuitement et sur une base non exclusive. Arrow Studio a le droit de contester certains des liens vers son site web dans le cas où le demandeur qui a l'intention d'activer les liens à arrowstudio.ch a, dans le passé, adopté des pratiques commerciales déloyales ou d'affaires qui ne sont pas généralement adopté ou accepté par le marché opérateurs, ou a fait des activités de concurrence déloyale vis-à-vis du fournisseur ou les fournisseurs de ce dernier, ou lorsque Arrow Studio craint que ces pratiques ou ces activités puissent être adoptées par le demandeur à l'avenir.\r\n
    \r\n
    \r\n
    <strong>5. Avertissements sur les contenus</strong>\r\n
    \r\n
    Arrow Studio ne garantit pas que le contenu du site Web soit approprié ou licites dans d'autres pays en dehors de la Suisse. Toutefois, dans le cas où ces contenus sont réputés être illicite ou illégale dans certains de ces pays, veuillez ne pas accéder à ce site web et ou si vous choisissez néanmoins d'y accéder, nous vous informons par la présente que votre utilisation des services fournis par arrowstudio.ch sera sous votre responsabilité exclusive et personnelle. Arrow Studio a également adopté des mesures pour garantir que le contenu de arrowstudio.ch soit précis et ne contienne pas d'informations incorrectes ou périmées. Cependant, Arrow Studio ne peut pas être tenue responsable de l'exactitude et de l'exhaustivité du contenu, à l'exception de sa responsabilité délictuelle et de négligence grave prévue par la loi.\r\n
    En outre, Arrow Studio ne peut pas garantir que le site web fonctionne en continu, sans interruption ni erreurs dues à la connexion internet. En cas de problème dans l'utilisation de notre site web, veuillez écrire à l'adresse e-mail suivante: info@arrowstudio.ch. Dans le même temps, veuillez contacter votre fournisseur de services internet et vérifiez que chaque dispositif de connexion internet et de l'accès au contenu du Web soit correctement activé, y compris votre navigateur internet. La nature dynamique du contenu internet et web peut ne pas permettre à arrowstudio.ch de fonctionner sans suspensions, interruptions ou discontinuité en raison de la mise à jour du site web. Arrow Studio a adopté des mesures de sécurité techniques et organisationnelles adéquates pour protéger les services sur arrowstudio.ch, l'intégrité des données et des communications électroniques afin d'empêcher l'utilisation non autorisée ou l'accès aux données, ainsi que pour prévenir les risques de diffusion, la destruction et la perte de données et des informations confidentielles/ non confidentielles en ce qui concerne les utilisateurs de arrowstudio.ch, et pour éviter tout accès non autorisé ou illicite de ces données et informations.\r\n
    \r\n
    \r\n
    <strong>6. Notre politique d'entreprise</strong>\r\n
    \r\n
    Arrow Studio a adopté une politique d'entreprise; sa mission consiste à vendre des produits à travers ses services et son site web pour «consommateur» seulement. «Consommateur» désigne toute personne physique qui agit sur arrowstudio.ch à des fins qui sont en dehors de son métier, activité commerciale ou professionnelle (le cas échéant). Si vous n'êtes pas un consommateur, veuillez ne pas utiliser nos services pour l'achat de produits sur arrowstudio.ch. Arrow Studio est en droit de s'opposer au traitement des ordres d'achat de personnes autres que les consommateurs et à tout autre ordre d'achat qui ne respecte pas les Conditions Générales de Vente et de ces conditions générales  d'utilisation.\r\n
    \r\n
    Notre but est de garantir votre entière satisfaction. Si, pour quelque raison que ce soit, vous n'êtes pas satisfait de votre commande, vous pouvez exercer votre droit de retour sur les produits achetés dans les quinze (15) jours à compter de la date à laquelle vous les avez reçus du vendeur. Les articles retournés doivent être expédiés au vendeur dans les quatorze (14) jours à compter de la date à laquelle vous avez informé le vendeur de votre décision de résilier le contrat. Les produits peuvent être retournés par l'envoi du colis par le biais de l'agent de livraison indiquée par le vendeur, ou par un autre agent. Nous vous invitons également à nous contacter sur info@arrowstudio.ch, afin que nous puissions vous fournir un numéro de retour (le vendeur vous enverra un e-mail confirmant la réception de la demande de retrait).\r\n
    \r\n
    \r\n
    <strong>7. Termes et conditions de retour</strong>\r\n
    \r\n
    Le droit au retour des produits est réputé exercé correctement une fois que les conditions suivantes ont été pleinement atteintes :
- email envoyé directement à info@arrowstudio.ch en indiquant explicitement votre décision de résilier le contrat et envoyé dans les quinze (15) jours après réception des produits;
- les produits ne doivent pas avoir été utilisés, portés ou lavés;
- l'étiquette d'identification doit encore être fixée aux produits, car elle constitue une partie intégrante de l'article;
- les produits retournés doivent l'être dans leur emballage d'origine;
- les produits retournés doivent être livrés à l'agent d'expédition dans les quatorze (14) jours suivant l'information au vendeur de votre décision de résilier le contrat;
- les produits ne doivent pas être endommagés.\r\n
    \r\n
    Si vous avez rempli toutes les exigences énoncées ci-dessus, le vendeur remboursera intégralement le prix des produits achetés retournés. Vous serez averti si les produits retournés ne peuvent pas être acceptés parce qu'ils ne respectent pas les conditions mentionnées dans le paragraphe précédent. Dans ce cas, vous pouvez choisir d'avoir les produits achetés relivrés chez vous, à vos frais. Si vous refusez la prestation ci-dessus, Arrow Studio Lausanne se réserve le droit de conserver les produits et le montant payé pour l'achat des produits.\r\n
    \r\n
    Veuillez noter que tous les droits, taxes et redevances que vous avez payés pour la livraison des produits achetés ne seront pas remboursés.\r\n
    \r\n
    \r\n
    <strong>8. Procédure et délai de remboursement </strong>\r\n
    \r\n
    Lorsque le vendeur a reçu les produits retournés et vérifié que les produits répondent à toutes les exigences, vous recevrez un e-mail vous informant que les produits retournés ont été acceptées. Quel que soit le mode de paiement que vous avez utilisé, la procédure de remboursement va commencer dans les quatorze (14) jours à partir du moment où le vendeur a été informé de votre décision d'exercer votre droit de retourner les produits achetés et une fois que Arrow Studio a vérifié que la déclaration a été effectuée dans le respect avec les conditions ci-dessus.\r\n
    \r\n
    \r\n
    <strong>9. Tag d'identification</strong>\r\n
    \r\n
    Tous les produits vendus par le vendeur comprennent une étiquette d'identification fixée. Veuillez essayer les produits sans enlever l'étiquette. Les produits retournés sans l'étiquette ne seront pas acceptés.\r\n
    \r\n
    \r\n
    <strong><em>Veuillez lire notre Politique de Confidentialité attentivement.</em></strong>\r\n
    \r\n
    \r\n
    <strong>1. Notre politique</strong>\r\n
    \r\n
    Toute personne a droit à la protection de ses données personnelles. Arrow Studio Lausanne respecte le droit de ses utilisateurs d'être informés au sujet de la collecte et d'autres opérations impliquant leurs données personnelles. En utilisant des données qui peuvent directement ou indirectement de vous identifier personnellement, nous allons appliquer un principe de stricte nécessité. Pour cette raison, nous avons conçu arrowstudio.ch d'une manière telle que l'utilisation de vos données personnelles sera maintenue à un minimum et ne dépassera pas les fins pour lesquelles vos données personnelles ont été collectées et/ou traitées; nous ne traitons pas vos données personnelles lorsque nous pouvons vous fournir des services par le biais de l'utilisation de données anonymes (comme la recherche de marketing faites pour améliorer nos services) ou par d'autres moyens qui permettent Arrow Studio Lausanne pour vous identifier, sauf lorsque cela est strictement nécessaire ou sur demande par les autorités publiques compétentes ou la police (par exemple, dans le cas des données de trafic ou votre adresse IP).\r\n
    Arrow Studio Lausanne détermine les finalités et les moyens du traitement de vos données personnelles, y compris les mesures de sécurité.\r\n
    Cette politique de confidentialité vous fournit toutes les informations nécessaires pour comprendre comment nous recueillons des données qui peuvent identifier les utilisateurs de arrowstudio.ch. Pour de plus amples informations sur notre politique de confidentialité, veuillez nous contacter au info@arrowstudio.ch.\r\n
    \r\n
    \r\n
    <strong>2. Comment nous utilisons les données personnelles et à quelles fins</strong>\r\n
    \r\n
    Vos données personnelles seront traitées aux fins suivantes:\r\n
    \r\n
    Lorsque vous vous inscrivez à notre site, nous recueillons vos données personnelles afin de vous fournir des services dans les zones d'accès réservées de arrowstudio.ch et de vous envoyer notre lettre d'information, seulement sur votre demande.\r\n
    \r\n
    Vos données personnelles peuvent être divulguées à des tiers lorsque cela est nécessaire pour traiter une commande.\r\n
    \r\n
    Nous tenons à vous informer qu'Arrow Studio Lausanne doit traiter les données personnelles de ses utilisateurs à des fins qui sont strictement liés à la fourniture de services sur arrowstudio.ch, à l'exécution des contrats relatifs à la vente et à l'achat de produits sur arrowstudio.ch.\r\n
    \r\n
    \r\n
    <strong>3. Mesures de sécurité</strong>\r\n
    \r\n
    Nous avons adopté des mesures de sécurité pour protéger les données à caractère personnel contre la destruction accidentelle ou illicite, la perte accidentelle, l'altération, la divulgation ou l'accès non autorisé, et contre toutes les autres raisons pour le traitement des données qui ne sont pas conformes à notre Politique de confidentialité. Néanmoins, Arrow Studio Lausanne ne peut pas garantir que les mesures de sécurité adoptées pour la protection du site web et de transmission de données et de l'information sur arrowstudio.ch empêcheront ou excluront tout risque d'accès non autorisé ou de perte de données.\r\n
    Il est souhaitable que votre ordinateur soit équipé de dispositifs de logiciels qui protègent les données du réseau de transmission/réception (tels que les systèmes antivirus mises à jour) et que votre fournisseur de services internet prenne des mesures appropriées pour la sécurité de la transmission de données en réseau (tels que, par exemple, les pare-feu et filtrage anti-spam).\r\n
    Nous tenons à vous informer que arrowstudio.ch peut traiter vos données personnelles sans votre consentement dans certaines circonstances prévues par la loi.\r\n
    \r\n
    \r\n
    <strong>4. Cookies</strong>\r\n
    \r\n
    arrowstudio.ch utilise des systèmes automatiques de collecte de données, comme les cookies. Un cookie est un dispositif qui est transmis sur le disque dur d'un utilisateur d'internet; il ne contient pas d'informations compréhensibles mais permet de relier un utilisateur d'internet à ses renseignements personnels fournis par l'utilisateur sur arrowstudio.ch. Les cookies sont diffusés par nos serveurs, et nul ne peut avoir accès aux informations qui y sont contenues. Seul Arrow Studio Lausanne traite l'information recueillie par les cookies, de manière collective et anonyme, afin d'optimiser ses services et son site web pour les besoins et les préférences de ses utilisateurs. Nous avons fourni des cookies dans le cadre de fonctions telles que la sélection de la langue, le parcours du catalogue, l'achat de produits. Comme vous le savez, chaque navigateur internet permet la suppression des cookies après chaque session. Votre navigateur internet contient des instructions sur ces procédures de suppression. Veuillez accéder à l'information sur votre navigateur internet si vous souhaitez supprimer les cookies. L'acceptation des procédures automatiques de collecte de données et l'utilisation des cookies sont nécessaires pour utiliser le site Web et de ses services, y compris l'achat de produits. Si vous avez commencé la procédure de suppression des cookies, Arrow Studio Lausanne ne peut pas garantir que toutes les pages web seront affichés ou que certains services seront fournis tels que, par exemple, le stockage ou l'affichage sur les pages web des produits que vous avez choisi lorsque vous acheviez les processus d'achat en ligne.";

    // ORDER CALLBACK

    $lang['SUCCESS_LINK_SHOP_TEXT'] = "Retour à la boutique";
    $lang['SUCCESS_TEXT'] = "Merci pour vos achats sur <a href=\"http://arrowstudio.ch\">arrowstudio.ch</a>";
    $lang['SUCCESS_TITLE'] = "Paiement réussi !";

    $lang['FAILURE_LINK_CART_TEXT'] = "Retour au panier";
    $lang['FAILURE_TEXT'] = "Votre paiement a été refusé. Réessayez ultérieurement ou contactez <a href=\"mailto:orders@arrowstudio.ch\">orders@arrowstudio.ch</a>";
    $lang['FAILURE_TITLE'] = "Paiement annulé !";
