<?php
	/**
	 * English language file
	 * @author Antoine De Gieter
	 */

	$lang = Array();
	$lang['LANG'] = "en-GB";

    /* DATE */
    $lang['DAY_OF_THE_WEEK'] = array(
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    );

    $lang['MONTH'] = array(
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    );

	/* BUTTONS */
	$lang['BUTTON_DOWNLOAD'] = "Download";

	/* MENU */
	$lang['MENU_COLLECTIONS'] = "Collections";
	$lang['MENU_SHOP'] = "Shop";
	$lang['MENU_SHOP_PAP'] = "Ready-to-wear";
	$lang['MENU_SHOP_ACC'] = "Accessories";
    $lang['MENU_SHOP_TSHIRT'] = "T-Shirt";
    $lang['MENU_SHOP_SWEATSHIRT'] = "Sweatshirt";
    $lang['MENU_SHOP_SKIRT'] = "Skirt";
    $lang['MENU_SHOP_BAG'] = "Bag";
	$lang['MENU_UNIVERSE'] = "Universe";
	$lang['MENU_ABOUT'] = "About";
	$lang['MENU_PHILOSOPHY'] = "Philosophy";
	$lang['MENU_DESIGNER'] = "Designer";
	$lang['MENU_BLOG'] = "Blog";
	$lang['MENU_CONTACT'] = "Contact";
	$lang['MENU_CART'] = "Cart";

	/* FOOTER */
	$lang['FOOTER_LEGAL_NOTICE'] = "Legal notice";
	$lang['FOOTER_PRESS'] = "Press";
	$lang['FOOTER_CREDITS'] = "Credits";

	/* SHOP */
	$lang['SHOP_ADD_TO_CART'] = "Add to cart";

	/* CART */
	$lang['CART_CONTENTS'] = "Cart";
	$lang['CART_CHECKOUT'] = "Check out";
	$lang['CART_EMPTY'] = "Empty";
	$lang['CART_TOTAL'] = "Total";
	$lang['CART_CONFIRM_EMPTY'] = "Are you sure?";
	$lang['CART_CONFIRM_REMOVE_ITEM'] = "Are you sure?";
	$lang['CART_BACK_TO_SHOP'] = "Back to the shop";

	/* CHECKOUT */
	$lang['CHECKOUT_TITLE'] = "Check out";
	$lang['CHECKOUT_FIRST_NAME'] = "First name";
    $lang['CHECKOUT_LAST_NAME'] = "Last name";
    $lang['CHECKOUT_EMAIL'] = "Email address";
    $lang['CHECKOUT_TITLES'] = array(
        "Mr", "Mrs", "Ms"
    );
	$lang['CHECKOUT_BILLING_ADDRESS'] = "Billing address";
	$lang['CHECKOUT_SHIPPING_ADDRESS'] = "Shipping address";
	$lang['CHECKOUT_PROCEED'] = "Proceed to payment";
	$lang['CHECKOUT_COPY'] = "Copy";

    /* ABOUT */
	$lang['PHILOSOPHY_TITLE'] = "Philosophy";
    $lang['PHILOSOPHY_TEXT'] = "Arrow Studio would like to see this consumption excess in which we live slow down.\r\n\r\nThe seasons are accelerating and fashion propagates faster and faster, which leads to more consumption and waste. We know it is sometimes difficult to become aware of this state of things and its consequences. This is why it is important to us to create timeless and quality clothing.\r\n\r\nHaving a French production line which allow us to combine proximity production, quality and know-how is essential.";
    $lang['DESIGNER_TITLE'] = "Mélanie Begg";
    $lang['DESIGNER_TEXT'] = "Arrow Studio is the creation of Swiss-British designer Melanie Begg, who founded the brand in Lausanne (Switzerland) in 2013 in order to express a very precise vision of fashion.\r\n\r\nIn her artistic work, Melanie draws inspiration from everyday life, street, music, design, architecture, and a minimalist lifestyle.";
    $lang['ABOUT_TITLE'] = "About us";
    $lang['ABOUT_TEXT'] = "Arrow Studio, founded in Lausanne (Switzerland) in 2013, designs clothes for women." . PHP_EOL . PHP_EOL
        . "The Arrow Studio style is defined by structured yet bold minimalism, a tribute to ‘effortless spirit’, with its clean-cut tailoring and high-quality fabrics. Materials are selected with great care and the garments made in France, to ensure the best possible balance between quality and expertise in an environmentally friendly process." . PHP_EOL . PHP_EOL
        . "Arrow Studio works with black and white predominantly, combined for a style that is simple and smooth. Enduring, essential pieces that are a tasteful woman’s must-have." . PHP_EOL . PHP_EOL
        . "Through a distinct color-palette and sharp lines, artistic director Mélanie Begg intends to create classic pieces that will not go out of style; whereas the choice of materials allows these designs to be worn year-round. With regards to seasons or passing years, Arrow Studio’s style is timeless.";


    /* MANAGER */
    $lang['MANAGER_TITLE'] = "Manager";
    $lang['MANAGER_RESTRICTED_ACCESS'] = "Restricted access";
    $lang['MANAGER_PASSWORD'] = "Password";
	$lang['MANAGER_SUBMIT'] = "Submit";
	$lang['MANAGER_CARTS'] = "Carts";
	$lang['MANAGER_ITEMS'] = "Items";

	/* UNIVERSE */
	$lang['UNIVERSE_TITLE'] = "Our universe defined in a few words";
	$lang['UNIVERSE_DEFINITIONS'] = array(
		"<strong>Basic, n. m.</strong> Essential garment for everyday life.",
		"<strong>Effortless, adj.</strong> Describes a style that requires nothing superfluous.",
		"<strong>Garçonne, n. f.</strong> Girl with the appearance of a boy.",
		"<strong>Intemporel, adj.</strong> Which is independent of time, which does not vary with him; everlasting.",
		"<strong>Minimalisme, n. m.</strong> <strong>1.</strong> Artistic and aesthetic style that emphasizes clean lines and simple shapes. <strong>2.</strong> Lifestyles, philosophy that puts simplicity as an essential value."
	);

	/* CREDITS */
	$lang['CREDITS_TITLE'] = "Credits";
	$lang['CREDITS_CONTENTS'] = array(
		array(
			"title" => "Photographer",
			"people" => array(
				array(
					"name" => "Clément Ardin",
					"url" => "http://a-c-portfolio.tumblr.com",
				),
				array(
					"name" => "Matthieu Spohn",
					"url" => "http://www.matthieu-spohn.com",
				),
			)
		),
        array(
            "title" => "Graphist",
            "people" => array(
                array(
                    "name" => "Céline Tissot",
                    "url" => "http://celinetissot.ch"
                )
            )
        ),
		array(
			"title" => "Model",
			"people" => array(
				array(
					"name" => "Anna Noélia Garcia",
					"url" => "https://www.facebook.com/annanoelia.garcia",
				),
				array(
					"name" => "Sandy Vaz",
					"url" => "",
				),
				array(
					"name" => "Joelle Catin",
					"url" => "",
				),
			)
		),
		array(
			"title" => "Web",
			"people" => array(
				array(
					"name" => "Net Production",
					"url" => "http://www.net-production.ch",
				),
			)
		)
	);

	/* LEGAL NOTICE */
	$lang['LEGAL_NOTICE_TEXT'] = "<strong>Welcome to our web site <a href=\"www.arrowstudio.ch\" class=\"link-black\">arrowstudio.ch</a>.</strong>\r\n
	These General Terms and Conditions of Use are governed by swiss law and in particular by \"Code des Obligations Suisses\". The access to and use of this web site as well as the purchase of products on arrowstudio.ch are based on the assumption that these General Terms and Conditions of Use have been read, understood and accepted by you.\r\n
	For further information request: <a href=\"info@arrowstudio.ch\" class=\"link-black\">info@arrowstudio.ch</a>.\r\n
	The Provider may amend or simply update all or part of these General Terms and Conditions of Use. Therefore, you should regularly access this section on the web site in order to check the publication of the most recent and updated General Terms and Conditions of Use of arrowsutio.ch. If you do not agree to all or part of the arrowstudio.ch’s General Terms and Conditions of Use, please do not use our web site.\r\n
	The access to and use of arrowstudio.ch, including display of web pages, communication with Provider, downloading product information and making purchases on the web site, are carried out by our users exclusively for personal purposes, which should in no way be connected to any trade, business or professional activity. Remember that you will be liable for your use of arrowstudio.ch and its contents. The Provider shall not be considered liable for any use of the web site and its contents made by its users that is not compliant with the laws and regulations in force, without prejudice to Provider's liability for intentional torts and gross negligence.\r\n
	In particular, you will be liable for communicating information or data which is not correct, false or concerning third parties (in the event such third parties have not given their consent) as well as for any improper use of such data or information.\r\n

	<strong>1. Privacy Policy</strong>\r\n
	We recommend that you read the Privacy Policy which also applies in the event that users access to arrowstudio.ch and use the relevant services without making purchases. The Privacy Policy will help you understand how and for what purposes arrowstudio.ch collects and uses your personal data.\r\n

	<strong>2. Intellectual Property Rights</strong>\r\n
	All content included on this Site, such as works, images, pictures, dialogues, music, sounds, videos, documents, drawings, figures, logos, menus, web pages, graphics, colours, schemes, tools, fonts, designs, diagrams, layouts, methods, processes, functions and software (collectively, “Content”), is the property of Arrow Studio Lausanne or Provider and is protected by national and international copyright and other intellectual property laws. You may not reproduce, publish, distribute, display, modify, create derivative work from, or exploit in any way, in whole or in part, the Content without the prior express written consent of Arrow Studio Lausanne and Provider as the case may be.
	Arrow Studio Lausanne and/or Provider shall have the exclusive right to authorize or prohibit in their sole discretion any reproduction, publication, distribution, display, modification, creation of derivative work from, or exploitation in any way of, in whole or in part, the Content. Arrow Studio Lausanne and/or Provider shall have the right, at any time, to claim the authorship of any Content posted on this Site and to object to any use, distortion or other modification of such Content.
	Any reproduction, publication, distribution, display, modification, creation of derivative work from, or exploitation in any way of, the Content expressly authorized in writing by Arrow Studio Lausanne and Provider shall be carried out by you for lawful purposes only and in compliance with all applicable laws.\r\n

	<strong>3. Links to Other Web Sites</strong>\r\n
	arrowstudio.ch may contain links to other web sites which are in no way connected to arrowstudio.ch or the Provider. The Provider does not control or monitor such third party web sites or their contents. The Provider shall not be held liable for the contents of such sites and/or for the rules adopted by them in respect of, but not limited to, your privacy and the processing of your personal data when you are visiting those web sites. Please, pay attention when you access these web sites through the links provided on arrowstudio.ch and carefully read their terms and conditions of use and their privacy policies. Our General Terms and Conditions of Use and Privacy Policy do not apply to the web sites of third parties. arrowstudio.ch provides links to other web sites exclusively to help its users in searching and surfing the Internet and to allow links to web sites on the Internet. When the Provider provides links to other web sites, the Provider does not recommend that to its users access these web sites and it does not provide any guarantees to their web content or to services and products supplied and sold by these web sites to Internet users.\r\n

	<strong>4. Links to arrowstudio.ch</strong>\r\n
	Please, contact the Provider at the following e-mail address: info@arrowstudio.ch if you are interested in linking the Home page of arrowstudio.ch and other web pages which can be publicly accessible. You are requested to contact the Provider for requesting our consent to linking arrowstudio.ch. The Provider grants links to arrowstudio.ch free of charge and on a non-exclusive basis. The Provider is entitled to object to certain links to its web site in the event that the applicant who intends to activate links to arrowstudio.ch has, in the past, adopted unfair commercial or business practices which are not generally adopted or accepted by the market operators, or has made unfair competition activities vis-à-vis the Provider or the latter's suppliers, or when the Provider fears that such practices or such activities might be adopted by the applicant in the future.\r\n

	<strong>5. Disclaimers on Content</strong>\r\n
	The Provider does not warrant that the contents of the web site are appropriate or lawful in other Countries outside Switzerland. However, in the event that such contents are deemed to be unlawful or illegal in some of these Countries, please do not access this web site and, where you nonetheless choose to access it, we hereby inform you your use of the services provided by arrowstudio.ch shall be your exclusive and personal responsibility. The Provider has also adopted measures to ensure that the content of arrowstudio.ch is accurate and does not contain any incorrect or out-of-date information. However, the Provider cannot be held liable for the accuracy and completeness of the content, except for its liability for tort and gross negligence and as otherwise provided for by the law.
	Moreover, the Provider cannot guarantee that the web site will operate continuously, without any interruptions and errors due to the connection to the Internet. In the event of any problem in using our web site please write to the following e-mail address: info@arrowstudio.ch. A Provider representative will be at your disposal to assist and help you to restore your access to the web site, as far as possible. At the same time, please contact your Internet services provider or check that each device for Internet connection and access to web content is correctly activated, including your Internet browser. The dynamic nature of the Internet and web content may not allow arrowstudio.ch to operate without any suspensions, interruptions or discontinuity due to updating the web site. The Provider has adopted adequate technical and organisational security measures to protect services on arrowstudio.ch, integrity of data and electronic communications in order to prevent unauthorised use of or access to data, as well as to prevent risks of dissemination, destruction and loss of data and confidential/non confidential information regarding users of arrowstudio.ch, and to avoid unauthorised or unlawful access to such data and information.\r\n

	<strong>6. Our Business Policy</strong>\r\n
	The Provider has adopted a business policy; its mission consists of selling products through its services and its web site to \"consumer\" only. \"Consumer\" shall mean any natural person who is acting on arrowstudio.ch for purposes which are outside his or her trade, business or professional activity (if any). If you are not a consumer, please do not use our services for purchasing products on arrowstudio.ch. The Provider shall be entitled to object to the processing of purchase orders from persons other than consumers and to any other purchase order which does not comply with the General Terms and Conditions of Sale and these General Terms and Conditions of Use.\r\n
	Our purpose is to guarantee your full satisfaction. If, for any reason whatsoever, you are not satisfied with your order, you may exercise your right to return purchased products within fifteen (15) days from the date on which you received them from the Vendor. Returned items must be shipped back to the Vendor within fourteen (14) days from the date on which you informed the Vendor of your decision to withdraw from the contract. The products may be returned by dispatching the package through the shipping agent indicated by the Vendor, or through another shipping agent. We also invite you to mail us on info@arrowstudio.ch, in order for us to provide you with a Return Number (the Vendor will send you an e-mail confirming receipt of the withdrawal request.\r\n

	<strong>7. Terms and Conditions of Return</strong>\r\n
	The right to return products shall be deemed correctly exercised once the following conditions have been fully met:
- email sent directly to info@arrowstudio.ch explicitly stating your decision to withdraw from the contract and sent within fifteen (15) days after receiving the products;
- the products must not have been used, worn or washed;
- the identification tag must still be attached to the products, which constitutes an integral part of the item;
- the returned products must be in their original packaging;
- the returned products must be delivered to the shipping agent within fourteen (14) days of when you informed the Vendor of your decision to withdraw from the contract;
- the products must not be damaged.\r\n
	If you have fulfilled all requirements set forth above, the Vendor will fully refund the price of the returned purchased products. You will be notified if the returned products cannot be accepted because they do not comply with the conditions indicated in the previous paragraph. In this case, you may choose to have the purchased products redelivered to you at your expense. If you refuse the above delivery, Arrow Studio Lausanne reserves the right to retain the products and the amount paid for your purchase of the products.\r\n
	Please note that any duties, taxes and fees you have paid for the delivery of the purchased products shall not be refunded.\r\n

	<strong>8. Refund Times and Procedures</strong>\r\n
	After the Vendor has received the returned products and checked that the products meet all requirements, you will receive an e-mail that the returned products have been accepted. Whatever form of payment you used, the refund procedure will start within fourteen (14) days from when the Vendor was informed of your decision to exercise your right to return the purchased products and once the Vendor has checked that the return was carried out in compliance with the above conditions.\r\n

	<strong>9. Identification Tag</strong>\r\n
	All products sold by the Vendor include an identification tag attached. Please try on the products without removing the tag and. Returned products without the tag will not be accepted.\r\n

	<strong><em>Please read our Privacy Policy carefully.</em></strong>\r\n

	<strong>1. Our Policy</strong>\r\n
	Everyone has the right to protection of his/her personal data. Arrow Studio Lausanne respects its users' right to be informed regarding the collection of and other operations involving their personal data. In using data that may directly or indirectly identify you personally, we will apply a principle of strict necessity. For this reason, we have designed arrowstudio.ch in such a way that the use of your personal data will be kept to a minimum and will not exceed the purposes for which your personal data was collected and/or processed; we do not process your personal data when we can provide you with services through the use of anonymous data (such as marketing research made for improving our services) or by other means which allow Arrow Studio Lausanne. to identify you, apart from when it is strictly necessary or upon request by competent public authorities or the police (for example, in case of traffic data or your IP address).
	Arrow Studio Lausanne determines the purposes and means for processing your personal data, including security measures
	This Privacy Policy provides you with all information needed to understand how we collect data which may identify arrowstudio.ch's users. For further information on our Privacy Policy, please contact us at info@arrowstudio.ch.\r\n

	<strong>2. How We Use Personal Data and for What Purposes</strong>\r\n
	In particular, your personal data shall be processed for the following purposes:
	when you register with our website we collect your personal data in order to provide you with services in arrowstudio.ch’s reserved access areas and to send you our Newsletter, when specifically requested.\r\n
	Your personal data may only be disclosed to third parties when it is necessary for processing an order.\r\n
	We wish to inform you Arrow Studio Lausanne shall process its users' personal data for purposes that are strictly connected to the supply of services on arrowstudio.ch, execution of contracts related to the sale and purchase of products on arrowstudio.ch.\r\n

	<strong>3. Security Measures</strong>\r\n
	We have adopted security measures to protect personal data against accidental or unlawful destruction, accidental loss, alteration, unauthorised disclosure or access, and against all other reasons for data processing that do not comply with our Privacy Policy. Nevertheless, Arrow Studio Lausanne cannot guarantee that the security measures adopted for the protection of the web site and for data and information transmission on arrowstudio.ch will prevent or exclude any risk of unauthorised access or of loss of data.
	It is advisable that your computer be provided with software devices that protect network data transmission/receipt (such as updated antivirus systems) and that your Internet service provider take appropriate measures for the security of network data transmission (such as, for example, firewalls and anti-spam filtering).
	We wish to inform you that arrowstudio.ch may process your personal data without your consent in certain circumstances provided by law.\r\n

	<strong>4. Cookies</strong>\r\n
	arrowstudio.ch uses automatic systems of data collection, such as cookies. A cookie is a device transmitted to the hard disk of an Internet user; it does not contain intelligible information but it allows linking between an Internet user to his/her personal information provided by the user on arrowstudio.ch. Cookies are disseminated by our servers, and no one may gain access to the information contained therein. Only Arrow Studio Lausanne process information collected by cookies, in a collective and anonymous way, in order to optimise its services and its web site for the needs and preferences of its users. We have provided cookies in connection to functions such as selecting the language, browsing the catalogue, purchasing products. As you know, each Internet browser allows the deletion of cookies after each session. Your Internet browser contains instructions on these deletion procedures. Please access the information on your Internet browser if you wish to delete cookies. The acceptance of automatic data collection procedures and the use of cookies are necessary for using the web site and its services, including the purchase of products. If you have started the procedure of deleting cookies, Arrow Studio Lausanne cannot ensure that all web pages will be displayed or that certain services will be supplied such as, for example, storage or display on the web pages of products you chose when you were finalising the online purchase processes.";

	// ORDER CALLBACK

    $lang['SUCCESS_LINK_SHOP_TEXT'] = "Back to the shop";
    $lang['SUCCESS_TEXT'] = "THANK YOU FOR SHOPPING AT <a href=\"http://arrowstudio.ch/\">arrowstudio.ch</a>";
    $lang['SUCCESS_TITLE'] = "Success!";

    $lang['FAILURE_LINK_CART_TEXT'] = "Back to the cart";
    $lang['FAILURE_TEXT'] = "YOUR PAYEMENT WAS REFUSED. TRY AGAIN LATER OR CONTACT <a href=\"mailto:orders@arrowstudio.ch\">orders@arrowstudio.ch</a>";
    $lang['FAILURE_TITLE'] = "Cancelled!";
