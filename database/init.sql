--
-- @author Antoine De Gieter
-- @copyright Net Production Köbe & Co
--

SET autocommit = 0;

START TRANSACTION;

--
-- Tables
--

CREATE TABLE language (
	`code` VARCHAR(5) NOT NULL,
	`label` VARCHAR(15),
	PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET="utf8";

CREATE TABLE customer (
	`id` INT AUTO_INCREMENT,
	`email` VARCHAR(63) NOT NULL,
	`password` VARCHAR(40) DEFAULT "",
	`title` ENUM("M", "Mme", "Mle", "Mr", "Mrs", "Ms", "Dr") DEFAULT "M",
	`first_name` VARCHAR(31) DEFAULT "",
	`last_name` VARCHAR(31) DEFAULT "",
	`billing_address` VARCHAR(255) DEFAULT "",
	`shipping_address` VARCHAR(255) DEFAULT "",
	`language` VARCHAR(5) NOT NULL DEFAULT "fr-CH",
	PRIMARY KEY (`id`, `email`),
	CONSTRAINT `fk_customer_language`
		FOREIGN KEY (`language`)
		REFERENCES language(`code`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET="utf8";

CREATE TABLE category (
	`id` INT AUTO_INCREMENT,
	`key` TEXT NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET="utf8";

CREATE TABLE category_tr (
	`category` INT NOT NULL,
	`language` VARCHAR(5) NOT NULL,
	`text` VARCHAR(15),
	PRIMARY KEY (`category`, `language`),
	CONSTRAINT `fk_category_tr_category`
        FOREIGN KEY (`category`)
        REFERENCES category(`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_category_tr_language`
        FOREIGN KEY (`language`)
        REFERENCES language(`code`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET="utf8";

CREATE TABLE subcategory (
	`id` INT AUTO_INCREMENT,
	`key` TEXT NOT NULL,
	`category` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `fk_subcategory_category`
		FOREIGN KEY (`category`)
		REFERENCES category(`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET="utf8";

CREATE TABLE subcategory_tr (
	`subcategory` INT NOT NULL,
	`language` VARCHAR(5) NOT NULL,
	`text` VARCHAR(15),
	PRIMARY KEY (`subcategory`, `language`),
	CONSTRAINT `fk_subcategory_tr_subcategory`
        FOREIGN KEY (`subcategory`)
        REFERENCES subcategory(`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_subcategory_tr_language`
        FOREIGN KEY (`language`)
        REFERENCES language(`code`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET="utf8";

CREATE TABLE item (
	`id` INT AUTO_INCREMENT,
	`initial_price` DOUBLE NOT NULL,
	`price` DOUBLE NOT NULL,
	`sales` BOOLEAN DEFAULT 0,
	`subcategory` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `fk_item_subcategory`
        FOREIGN KEY (`subcategory`)
        REFERENCES subcategory(`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET="utf8";

CREATE TABLE item_tr (
	`item` INT NOT NULL,
	`language` VARCHAR(5) NOT NULL,
	`text` VARCHAR (15) NOT NULL,
	`description` TEXT,
	PRIMARY KEY (`item`, `language`),
	CONSTRAINT `fk_item_tr_item`
        FOREIGN KEY (`item`)
        REFERENCES item(`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
	CONSTRAINT `fk_item_tr_language`
        FOREIGN KEY (`language`)
        REFERENCES language(`code`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET="utf8";

CREATE TABLE item_picture (
	`item` INT NOT NULL,
	`picture` INT NOT NULL,
	PRIMARY KEY (`item`, `picture`),
	CONSTRAINT `fk_item_picture_item`
        FOREIGN KEY (`item`)
        REFERENCES item(`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET="utf8";

CREATE TABLE cart (
	`id` INT AUTO_INCREMENT,
	`dt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`billing_address` VARCHAR(255) DEFAULT "",
	`shipping_address` VARCHAR(255) DEFAULT "",
	`state` ENUM('pending', 'paid', 'shipped', 'cancelled') DEFAULT "pending",
	`customer` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `fk_cart_customer`
        FOREIGN KEY (`customer`)
        REFERENCES customer(`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET="utf8";

CREATE TABLE contains (
	`cart` INT NOT NULL,
	`item` INT NOT NULL,
	`quantity` INT DEFAULT 1,
	`size` ENUM('S', 'M', 'L', '-'),
	PRIMARY KEY (`cart`, `item`),
	CONSTRAINT `fk_contains_cart`
		FOREIGN KEY (`cart`)
		REFERENCES cart(`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `fk_contains_item`
		FOREIGN KEY (`item`)
		REFERENCES item(`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET="utf8";

COMMIT;
