--
-- @author Antoine De Gieter
-- @copyright Net Production Köbe & Co
--

START TRANSACTION;

INSERT INTO language VALUES ("fr-CH", "Français");
INSERT INTO language VALUES ("en-GB", "English");

INSERT INTO customer VALUES (1, "antoine.degieter@net-production.ch",
SHA1("893QQY"), "Antoine", "De Gieter",
"Montrinsans, route de Malpas,\n25160 Labergement Ste Marie\nFRANCE",
"Montrinsans, route de Malpas,\n25160 Labergement Ste Marie\nFRANCE"),
(2, "nathan.kobe@net-production.ch",
SHA1("893QQY"), "Nathan", "Köbe",
"Rue de Rive 35,\n1260 Nyon\nSUISSE",
"Rue de Rive 35,\n1260 Nyon\nSUISSE"),
(3, "oday.yehia@net-production.ch",
SHA1("893QQY"), "Oday", "Yehia",
"Avenue de la Chablière 21B,\n1004 Lausanne,\nSUISSE",
"Avenue de la Chablière 21B,\n1004 Lausanne,\nSUISSE");

INSERT INTO category VALUES (1, "pretaporter"), (2, "bags"), (3, "jewels");

INSERT INTO category_tr VALUES (1, "fr-CH", "Prêt-à-porter"), (1, "en-GB", "Ready-to-wear"), -- prêt-à-porter works in english too
(2, "fr-CH", "Sacs"), (2, "en-GB", "Bags"),
(3, "fr-CH", "Bijoux"), (3, "en-GB", "Jewels");

INSERT INTO subcategory VALUES (1, "tshirt", 1), (2, "skirt", 1), (3, "sweatshirt", 1), (4, "bag", 2);

INSERT INTO subcategory_tr VALUES (1, "fr-CH", "T-Shirt"), (1, "en-GB", "T-Shirt"),
(2, "fr-CH", "Jupe"), (2, "en-GB", "Skirt"),
(3, "fr-CH", "Sweatshirt"), (3, "en-GB", "Sweatshirt"),
(4, "fr-CH", "Sac"), (4, "en-GB", "Bag");

INSERT INTO item VALUES (1, 0, 269, 0, 1), (2, 0, 249, 0, 1),
(3, 0, 239, 0, 1), (4, 0, 1129, 0, 2), (5, 0, 1429, 0, 2),
(6, 0, 1389, 0, 2), (7, 0, 479, 0, 3), (8, 0, 429, 0, 3),
(9, 0, 239, 0, 4), (10, 0, 69, 0, 4);

INSERT INTO item_picture VALUES (1, 1), (1, 2), (1, 3),
(2, 1), (2, 2), (2, 3),
(3, 1), (3, 2), (3, 3),
(4, 1), (4, 2), (4, 3),
(5, 1), (5, 2), (5, 3),
(6, 1), (6, 2), (6, 3),
(7, 1), (7, 2), (7, 3),
(8, 1), (8, 2), (8, 3),
(9, 1), (9, 2), (9, 3),
(10, 1), (10, 2), (10, 3), (10, 4), (10, 5);

INSERT INTO `item_tr` (`item`, `language`, `text`, `description`) VALUES
(1, 'en-GB', 'Thalamus', 'Details and style:\r\nMade out of cotton-cashmere jersey, the “Thalamus” t-shirt is already a classic! Wear it with a skirt or a pair of jeans, under a blazer.\r\n- black cotton-cashmere stretch jersey\r\n- easy to slip on\r\n- 79% cotton, 14% cashmere, 7% elastane\r\n- delicate, wash cold\r\n\r\nSize and cut:\r\n- chose your normal size\r\n- crop cut\r\n- medium thickness of material, stretch\r\n- The model is 178cm tall and is wearing a size M'),
(1, 'fr-CH', 'Thalamus', 'Détails et style :\r\nConfectionné dans un jersey coton-cachemire, ce t-shirt « thalamus »\r\nest une pièce incontournable de toutes les garde-robes contemporaines.\r\nPortez ce modèle avec une jupe ou un jeans, sous un blazer.\r\n- Jersey de coton-cachemire stretch noir\r\n- S''enfile simplement\r\n- 79 % coton, 14 % cachemire, 7 % élasthanne\r\n- Couleur: Noir\r\n- Nettoyage à froid, délicat\r\n\r\nTaille et coupe :\r\n- Taille normalement, prenez votre taille habituelle\r\n- Coupe droite\r\n- Tissu d’épaisseur moyenne, stretch\r\n- Le mannequin mesure 178cm et porte une taille M'),
(2, 'en-GB', 'Trigone', 'Details and style:\r\nMade out of delightfully soft cotton-cashmere jersey, this top has asymmetrical sleeves. This sleek piece adds a touch of elegance to any outfit. Wear with tight-fitted trousers for best effect.\r\n- black cotton-cashmere stretch jersey\r\n- easy to slip on\r\n- 79% cotton, 14% cashmere, 7% elastane\r\n- delicate, wash cold\r\n\r\nSize and cut:\r\n- chose your normal size\r\n- crop cut\r\n- medium thickness of material, stretch\r\n- The model is 178cm tall and is wearing a size M'),
(2, 'fr-CH', 'Trigone', 'Détails et style :\r\nConfectionné en jersey coton-cachemire confortable, ce haut fabriqué en France possède des manches asymétriques.\r\nCette pièce épurée rehaussera votre garde-robe contemporaine.\r\nPortez-le avec un pantalon ajusté pour mettre en valeur sa coupe.\r\n- Jersey de coton-cachemire stretch noir\r\n- S''enfile simplement\r\n- 79 % coton, 14 % cachemire, 7 % élasthanne\r\n- Couleur: noir\r\n- Nettoyage à froid, délicat\r\n\r\nTaille et coupe :\r\n- Taille normalement, prenez votre taille habituelle\r\n- Coupe droite\r\n- Tissu d’épaisseur moyenne, stretch\r\n- Le mannequin mesure 178cm et porte une taille M'),
(3, 'en-GB', 'Thoracique', 'Details and style:\r\nMade out of cotton-cashmere jersey, the “Thoracique” t-shirt is already a classic! Wear it with a skirt or a pair of jeans, under a blazer.\r\n- black cotton-cashmere stretch jersey\r\n- easy to slip on\r\n- 79% cotton, 14% cashmere, 7% elastane\r\n- delicate, wash cold\r\n\r\nSize and cut:\r\n- chose your normal size\r\n- crop cut\r\n- medium thickness of material, stretch\r\n- The model is 178cm tall and is wearing a size M'),
(3, 'fr-CH', 'Thoracique', 'Détails et style :\r\nConfectionné dans un jersey coton-cachemire, ce t-shirt « thoracique »\r\nest une pièce incontournable de toutes les garde-robes contemporaines.\r\nPortez ce modèle avec une jupe ou un jeans, par expemple sous un blazer.\r\n- Jersey de coton-cachemire stretch noir\r\n- S''enfile simplement\r\n- 79 % coton, 14 % cachemire, 7 % élasthanne\r\n- Couleur: Noir\r\n- Nettoyage à froid, délicat\r\n\r\nTaille et coupe :\r\n- Taille normalement, prenez votre taille habituelle\r\n- Coupe courte et large\r\n- Tissu d’épaisseur moyenne, stretch\r\n- Le mannequin mesure 178cm et porte une taille M'),
(4, 'en-GB', 'Jugulaire', 'Details and style:\r\nSleek and comfortable, the new “Jugulaire” skirt will definitely be your new favorite! Made in France out of fine and very soft leather, does not stretch\r\n- The model is 178cm tall and is wearing a size M'),
(4, 'fr-CH', 'Jugulaire', 'Détails et style :\r\nModerne et pratique, cette jupe « jugulaire » sera votre nouveau basique de prédilection. Confectionné en France dans un cuir doux et fin, ce modèle avec empiècements noirs est doté d’un zip au milieu dos.\r\nPortez-la en toutes circonstances avec une chemise ou un t-shirt et avec une basket ou un talon.\r\n- Matières: 100% cuir d''agneau\r\n- Couleurs: Noir et blanc\r\n- Fermeture à glissière dissimulée au milieu dos\r\n- Nettoyage par un professionnel\r\n\r\nTaille et coupe :\r\n- Taille normalement, prenez votre taille habituelle\r\n- Tombe sur le genou\r\n- Cuir souple, ne se détend pas\r\n- Le mannequin mesure 178cm et porte la taille M'),
(5, 'en-GB', 'Jumeau', 'Details and style:\r\nSleek and comfortable, the new “Jumeau” skirt will definitely be your new favorite! Made in France out of fine and very soft leather, this piece zips up in the back. Perfect for every occasion; wear it with a shirt or t-shirt, heels, flats or even sneakers.\r\n- lambskin\r\n- invisible back zipper\r\n- must be cleaned by a professional\r\n\r\nsize and cut:\r\n- chose your normal size\r\n- knee length\r\n- supple leather, does not stretch\r\n- The model is 178cm tall and is wearing a size M'),
(5, 'fr-CH', 'Jumeau', 'Détails et style :\r\nModerne et pratique, cette jupe « jumeau » sera votre nouveau basique de prédilection. Confectionné en France dans un cuir doux et fin, ce modèle bicolore\r\nest doté d’un zip sur l’arrière gauche. Portez-la en toutes circonstances avec une chemise ou un t-shirt et avec une basket ou un talon.\r\n- Matières: 100% cuir d''agneau\r\n- Couleurs: Noir et blanc\r\n- Fermeture à glissière dissimulée au milieu dos\r\n- Nettoyage par un professionnel\r\n- Fermeture à glissière dissimulée dans le dos à gauche\r\n- Nettoyage par un professionnel\r\n\r\nTaille et coupe :\r\n- Taille normalement, prenez votre taille habituelle\r\n- Tombe sur le genou\r\n- Cuir souple, ne se détend pas\r\n- Le mannequin mesure 178cm et porte la taille M'),
(6, 'en-GB', 'Joue', 'Details and style:\r\nSleek and comfortable, show off your legs in the new “Joue” skirt! Made in France out of fine and very soft leather, “Joue” has a sexy slit on the right thigh and zips up in the back. Perfect for every occasion; wear it with a shirt or t-shirt, heels, flats or even sneakers.\r\n- lambskin\r\n- invisible back zipper\r\n- must be cleaned by a professional\r\n\r\nsize and cut:\r\n- chose your normal size\r\n- knee length\r\n- supple leather, does not stretch\r\n- The model is 178cm tall and is wearing a size M'),
(6, 'fr-CH', 'Joue', 'Détails et style :\r\nModerne et pratique, cette jupe « joue » sera votre nouveau basique de prédilection.\r\nConfectionné en France dans un cuir doux et fin, ce modèle noir avec une découpe sur la cuisse gauche\r\nest doté d’un zip sur le côté gauche.\r\nPortez-la avec un t-shirt ou une chemise et une basket ou un talon.\r\n- 100% cuir d''agneau\r\n- Fermeture à glissière dissimulée sur le côté gauche\r\n- Nettoyage par un professionnel\r\n\r\nTaille et coupe :\r\n- Taille normalement, prenez votre taille habituelle\r\n- Tombe sur le genou\r\n- Cuir souple, ne se détend pas\r\n- Le mannequin mesure 178cm et porte la taille M'),
(7, 'en-GB', 'Palmaire', 'Details and style:\r\nBoost your wardrobe with the “Palmaire” sweatshirt. Made in France out of duffle, this asymmetrical sweater is perfect with the “Jumeau” skirt or a simple pair of jeans.\r\n- easy to slip on\r\n- 59% cotton, 22% wool, 22% modal\r\n- delicate, wash cold\r\n\r\nSize and cut:\r\n- chose your normal size\r\n- crop cut\r\n- medium thickness of material\r\n- The model is 178cm tall and is wearing a size M'),
(7, 'fr-CH', 'Palmaire', 'Détails et style :\r\nDynamisez vos basiques intemporels avec ce sweat « palmaire ».\r\nConfectionné en France dans un molleton noir, ce sweat asymétrique\r\nira parfaitement avec la jupe en cuir « jumeau » ou un jeans.\r\n- Matière: 56% coton - 22% laine - 22% modal\r\n- Couleur: Noir\r\n- S’enfile simplement\r\n- Lavage à froid, délicat\r\n\r\nTaille et coupe :\r\n- Taille normalement, prenez votre taille habituelle\r\n- Coupe ample\r\n- Tissu d’épaisseur moyenne\r\n- Le mannequin mesure 178cm et porte une taille M'),
(8, 'en-GB', 'Papille', 'Details and style:\r\nThe “Papille” sweatshirt is our reference when it comes to smooth, timeless pieces. Made in France out, this oversized black duffle sweatshirt goes with anything.\r\n\r\n- easy to slip on\r\n- 59% cotton, 22% wool, 22% modal\r\n- delicate, wash cold\r\n\r\nSize and cut:\r\n- chose your normal size\r\n- crop cut\r\n- medium thickness of material\r\n- The model is 178cm tall and is wearing a size M'),
(8, 'fr-CH', 'Papille', 'Détails et style :\r\nLe sweat « papille » est notre référence en matière de basiques moderne et intemporel.\r\nConfectionné en France dans un molleton noir, ce sweat ample est facile à coordonner avec une jupe ou un jeans.\r\n- Matière: 56% coton - 22% laine - 2% modal\r\n- Couleur: Noir\r\n- S’enfile simplement\r\n- Lavage à froid, délicat\r\n\r\nTaille et coupe :\r\n- Taille normalement, prenez votre taille habituelle\r\n- Coupe courte\r\n- Tissu d’épaisseur moyenne\r\n- Le mannequin mesure 178cm et porte une taille M'),
(9, 'en-GB', 'Leather plastic bag', 'Handcrafted in Lausanne from white smooth and textured leather, this Arrow Studio\'s minimalist and unique bag embodies the brand''s pure aesthetic.\r\n\r\n- 100% Leather\r\n- Dimension : 30x40 cm'),
(9, 'fr-CH', 'Leather plastic bag', 'Fabriqués à la main à Lausanne dans un cuir souple et texturé, ce sac Arrow Studio minimaliste et unique incarne l''esthétique pure de la marque.\r\n\r\n- 100% Cuir\r\n- Dimension : 30x40 cm'),
(10, 'en-GB', 'Scapula', 'Handcrafted in Lausanne, this Arrow Studio\'s minimalist and unique bag embodies the brand\'s pure aesthetic.\r\n\r\n- Dimension : 35x30 cm'),
(10, 'fr-CH', 'Scapula', 'Fabriqués à la main à Lausanne, ce sac Arrow Studio minimaliste et unique incarne l\'esthétique pure de la marque.\r\n\r\n- Dimension : 35x30 cm');

COMMIT;
